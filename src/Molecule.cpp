#include <boost/regex.hpp>
#include <algorithm>
#include <iostream>
#include <string>

#include "zcross/Element.hpp"
#include "zcross/Molecule.hpp"
#include "zcross/MoleculeComponent.hpp"
#include "zcross/PeriodicTable.hpp"
#include "zcross/Utils.hpp"
#include "zcross/ZCrossError.hpp"

using namespace dfpe;
using namespace std;
using namespace boost;
using namespace boost::units;



Molecule::Molecule(const Molecule &other)
{

    for (MoleculeComponent component: other.components)
        components.push_back(MoleculeComponent(component));

    for (int quantity: other.quantities)
        quantities.push_back(quantity);

    isomer = other.isomer;

    updateCache();
}

Molecule::Molecule(const Element &element, unsigned int quantity)
{
    components.push_back(MoleculeComponent(element));
    quantities.push_back(quantity);

    updateCache();
}



void Molecule::updateCache()
{
    cachedMass = QtySiMass();

    for (unsigned int i = 0; i < components.size(); ++i)
    {
        const MoleculeComponent &component = components[i];
        unsigned int            quantity   = quantities[i];

        if (component.isElement())
            cachedMass += component.getElement().getMass() * (double) quantity;
        else if (component.isMolecule())
            cachedMass += component.getMolecule().getMass() * (double) quantity;
    }
    
    cachedPolar = guessPolarity();

    cachedHash  = std::hash<Molecule>()(*this);
}

Molecule::Molecule(const std::string &formula)
{


    string::const_iterator start = formula.begin();
    string::const_iterator end   = formula.end();

    match_results<std::string::const_iterator> result;

    if (boost::regex_search(start, end, result, Utils::isomerRegex, regex_constants::match_continuous))
    {
        isomer = std::string(result[1].first, result[1].second);
        start = result[0].second;
    }

    while (start != end)
    {
        if (boost::regex_search(start, end, result, Utils::elementRegex, regex_constants::match_continuous))
        {

            std::string       symbol(result[1].first, result[1].second);
            MoleculeComponent moleculeComponent(PeriodicTable::getInstance().parseElement(symbol));


            std::string quantityStr(result[2].first, result[2].second);
            int         quantity;
            if (quantityStr.empty())
                quantity = 1;
            else
                quantity = stoi(quantityStr);


            int                                 posExisting = -1;
            vector<MoleculeComponent>::iterator itExisting  = find(components.begin(), components.end(), moleculeComponent);

            if (itExisting != components.end())
                posExisting = distance(components.begin(), itExisting);


            if (posExisting >= 0)
            {
                quantities[posExisting] += quantity;
            } else
            {
                components.push_back(moleculeComponent);
                quantities.push_back(quantity);
            }

            // update search position:
            start = result[0].second;
        } else if (*start == '(')
        {
            start++;

            int    level = 1;
            string submoleculeStr;

            while (start != end)
            {
                if (*start == '(')
                    level++;
                if (*start == ')')
                    level--;

                if (level > 0)
                    submoleculeStr += *start;

                start++;

                if (level == 0)
                    break;
            }

            if (!submoleculeStr.empty())
            {
                string quantityStr;

                while (start != end)
                {
                    if (isdigit(*start))
                        quantityStr += *start;
                    else
                        break;
                    start++;
                }


                int quantity;
                if (quantityStr.empty())
                    quantity = 1;
                else
                    quantity = stoi(quantityStr);
                Molecule          submolecule(submoleculeStr);
                MoleculeComponent moleculeComponent(submolecule);

                int                                 posExisting = -1;
                vector<MoleculeComponent>::iterator itExisting  = find(components.begin(), components.end(), moleculeComponent);

                if (itExisting != components.end())
                    posExisting = distance(components.begin(), itExisting);


                if (posExisting >= 0)
                {
                    quantities[posExisting] += quantity;
                } else
                {
                    components.push_back(moleculeComponent);
                    quantities.push_back(quantity);
                }
            }
        } else
            throw ZCrossError("Unable to parse molecule '" + formula + "'. It should have a format like: H2O, CH4, Si(CH3)4, etc.");

    }

    updateCache();
}

const std::string Molecule::toString(const PrintMode &mode) const
{
    if (mode == PrintMode::BASIC)
        return toBasicString();
    else if (mode == PrintMode::PRETTY)
        return toPrettyString();
    else if (mode == PrintMode::LATEX)
        return toLatexString();
    else
        throw ZCrossError("Unable to decode the provided PrintMode. Contact developers.");
}

const std::string Molecule::toBasicString() const
{
    stringstream ss;

    if (isomer.has_value())
        ss << *isomer << "-";

    for (unsigned int i = 0; i < components.size(); ++i)
    {
        const MoleculeComponent &component = components[i];
        unsigned int            quantity   = quantities[i];

        if (component.isElement())
            ss << component.getElement().getSymbol();
        else if (component.isMolecule())
            ss << "(" << component.getMolecule().toBasicString() << ")";

        if (quantity != 1)
            ss << quantity;
    }
    return ss.str();
}

const std::string Molecule::toPrettyString() const
{
    stringstream ss;

    if (isomer.has_value())
        ss << *isomer << "-";


    for (unsigned int i = 0; i < components.size(); ++i)
    {
        const MoleculeComponent &component = components[i];
        unsigned int            quantity   = quantities[i];

        if (component.isElement())
            ss << component.getElement().getSymbol();
        else if (component.isMolecule())
            ss << "(" << component.getMolecule().toPrettyString() << ")";

        if (quantity != 1)
            ss << Utils::getScriptedNumber((int) quantity, false);
    }
    return ss.str();
}

const std::string Molecule::toLatexString() const
{
    stringstream ss;

    if (isomer.has_value())
        ss << "\\text{" << *isomer << "}-";

    for (unsigned int i = 0; i < components.size(); ++i)
    {
        const MoleculeComponent &component = components[i];
        unsigned int            quantity   = quantities[i];

        if (component.isElement())
            ss << component.getElement().getSymbol();
        else if (component.isMolecule())
            ss << "\\left(" << component.getMolecule().toLatexString() << "\\right)";

        if (quantity != 1)
            ss << "_" << "{" << quantity << "}";
    }
    return ss.str();
}

unsigned int Molecule::getSize() const
{
    return components.size();
}

const QtySiMass &Molecule::getMass() const
{
    return cachedMass;
}

bool Molecule::operator==(const Molecule &rhs) const
{
    if (cachedHash != rhs.cachedHash)
        return false; // This will save a lot of work on 99% of cases

    if (components.size() != rhs.components.size())
        return false;

    for (unsigned int i = 0; i < components.size(); i++)
    {

        vector<MoleculeComponent>::const_iterator rhsIt = find(rhs.components.begin(), rhs.components.end(), components[i]);

        if (rhsIt == rhs.components.end())
            return false;

        int rhsPos = distance(rhs.components.begin(), rhsIt);

        if (quantities[i] != rhs.quantities[rhsPos])
            return false;
    }

    if (!isomer.has_value() && rhs.isomer.has_value() && rhs.isomer.value() != "n")
        return false;
    if (!rhs.isomer.has_value() && isomer.has_value() && isomer.value() != "n")
        return false;

    return true;
}

bool Molecule::operator!=(const Molecule &rhs) const
{
    return !(rhs == *this);
}


Molecule &Molecule::operator=(const Molecule &rhs)
{
    components.clear();
    quantities.clear();

    components.insert(components.begin(), rhs.components.begin(), rhs.components.end());
    quantities.insert(quantities.begin(), rhs.quantities.begin(), rhs.quantities.end());

    isomer = rhs.isomer;

    updateCache();

    return *this;

}

Molecule &Molecule::operator+=(const Molecule &rhs)
{
    for (unsigned int i = 0; i < rhs.components.size(); ++i)
    {
        const MoleculeComponent &rhsComponent = rhs.components[i];
        unsigned int            rhsQuantity   = rhs.quantities[i];

        bool found = false;

        for (unsigned int j = 0; j < components.size(); ++j)
        {
            if (components[j] == rhsComponent)
            {
                found = true;
                quantities[j] += rhsQuantity;
                break;
            }
        }

        if (!found)
        {
            components.push_back(rhsComponent);
            quantities.push_back(rhsQuantity);
        }
    }

    updateCache();

    return *this;
}

Molecule &Molecule::operator-=(const Molecule &rhs)
{
    for (unsigned int i = 0; i < rhs.components.size(); ++i)
    {
        const MoleculeComponent &rhsComponent = rhs.components[i];
        unsigned int            rhsQuantity   = rhs.quantities[i];

        for (unsigned int j = 0; j < components.size(); ++j)
        {
            if (components[j] == rhsComponent)
            {
                if (rhsQuantity < quantities[j])
                    quantities[j] -= rhsQuantity;
                else
                {
                    components.erase(components.begin() + j);
                    quantities.erase(quantities.begin() + j);
                }
                break;
            }
        }
    }


    updateCache();
    return *this;
}


Molecule dfpe::operator+(const Molecule &lhs, const Molecule &rhs)
{
    Molecule result(lhs);
    result += rhs;
    return result;
}

Molecule dfpe::operator-(const Molecule &lhs, const Molecule &rhs)
{
    Molecule result(lhs);
    result -= rhs;
    return result;
}

Molecule Molecule::compact()
{
    Molecule result("");

    for (unsigned int i = 0; i < components.size(); i++)
    {
        if (components[i].isElement())
            result += Molecule(components[i].getElement(), quantities[i]);
        else if (components[i].isMolecule())
        {
            Molecule submolecule = components[i].getMolecule().compact(); // We have a submolecule with only elements inside

            for (unsigned int j = 0; j < submolecule.getSize(); j++)
            {
                if (submolecule.components[j].isElement())
                    result += Molecule(submolecule.components[j].getElement(), submolecule.quantities[j] * quantities[i]);
                else
                    throw ZCrossError("Internal error: At this position the component should be only an Element. Contact developers.")
            }
        }
    }
    return result;
}

ostream& dfpe::operator<<(ostream &os, const Molecule &molecule)
{
    os << molecule.toString(PrintMode::BASIC);
    return os;
}

const MoleculeComponent &Molecule::getComponent(unsigned int component) const
{
    return components[component];
}

unsigned int Molecule::getQuantity(unsigned int component) const
{
    return quantities[component];
}


bool Molecule::isMonoatomic() const
{
	return components.size() == 1 && quantities[0] == 1;
}

bool Molecule::isIsomer() const
{
    return isomer.has_value();
}


bool Molecule::isDiatomic() const
{
	return components.size() == 2 && quantities[0] == 1 && quantities[1] == 1;
}

bool Molecule::isHomonuclear() const
{
	return components.size() == 1;
}

bool Molecule::isHeteronuclear() const
{
	return components.size() > 1;
}

bool Molecule::isPolar() const
{
	return cachedPolar;
}

bool Molecule::guessPolarity() const
{
	// Try to guess if the molecule is polar or nonpolar
	
	int s = components.size();
		
		
	// Empty molecules
	if (components.empty())
		return false;
	
	// Monoatomic molecules: A (eg. Ar, Xe, Ne, ...)
	if (isMonoatomic())
		return false;
	
	// Diatomic molecules of the same element: Aₓ (eg. O2, H2, ...)
	if (isHomonuclear())
		return false;
		
	// Linear molecules: AB (eg. CO, ...)
	if (isDiatomic() && isHeteronuclear())
		return true;
	
	// Molecules with a single H (eg. HF, ...)
	Element elementH = PeriodicTable::getInstance().parseElement("H");
	if (getElementQuantity(elementH) == 1)
		return true;
		
	// Molecules with an O at one end 
	Element elementO = PeriodicTable::getInstance().parseElement("O");
	if (components.front().isElement() && components.front().getElement() == elementO && quantities[0] == 1)
		return true;
	if (components.back().isElement()  && components.back().getElement()  == elementO && quantities[s-1] == 1)
		return true;
		
	// Molecules with an N at one end 
	Element elementN = PeriodicTable::getInstance().parseElement("N");
	if (components.front().isElement() && components.front().getElement() == elementN && quantities[0] == 1)
		return true;
	if (components.back().isElement()  && components.back().getElement()  == elementN && quantities[s-1] == 1)
		return true;
		
	// Molecules with an OH at one end 
	if (s >= 2)
	{ 
		if (components[s-1].isElement() && components[s-1].getElement() == elementH && quantities[s] == 1 &&
		    components[s-2].isElement() && components[s-2].getElement() == elementO && quantities[s] == 1)
			return true;
	}
	
	// Most carbon compounds
	Element elementC = PeriodicTable::getInstance().parseElement("C");
	if (getElementQuantity(elementC) > 0)
		return false;
		
	return true;
}

unsigned int Molecule::getElementQuantity(const Element& element) const
{
	int count = 0;
	
	int i = 0;
	for (const MoleculeComponent& component: components)
	{
		if (component.isElement() && component.getElement() == element)
			count += quantities[i];
		else if (component.isMolecule())
			count += component.getMolecule().getElementQuantity(element);
				
		i++; 
	}
	
	return count;
}

bool Molecule::operator<(const Molecule &rhs) const
{
	return getMass() < rhs.getMass();
}
