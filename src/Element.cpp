#include "zcross/Element.hpp"

using namespace std;
using namespace dfpe;

bool Element::operator<(const Element &rhs) const
{
    return z < rhs.z;
}

bool Element::operator>(const Element &rhs) const
{
    return z > rhs.z;
}

bool Element::operator<=(const Element &rhs) const
{
    return z <= rhs.z;
}

bool Element::operator>=(const Element &rhs) const
{
    return z >= rhs.z;
}

bool Element::operator==(const Element &rhs) const
{
    return z == rhs.z && mass == rhs.mass;
}

bool Element::operator!=(const Element &rhs) const
{
    return !(*this == rhs);
}

