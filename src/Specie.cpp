#include <zcross/ParticleSpecie.hpp>
#include <zcross/IonSpecie.hpp>
#include <zcross/Molecule.hpp>
#include <zcross/Utils.hpp>
#include <zcross/ZCrossError.hpp>
#include <boost/regex.hpp>
#include <boost/units/systems/detail/constants.hpp>
#include <boost/units/systems/si/codata/electromagnetic_constants.hpp>
#include <algorithm>
#include <vector>
#include <iostream>
#include <zcross/PDGTable.hpp>

#include "zcross/Specie.hpp"



using namespace dfpe;
using namespace boost;
using namespace std;

Specie::Specie(const std::string &formula)
{
    optional<ParticleSpecie> particleSpecie = PDGTable::instance.get(formula);
	if ( particleSpecie.has_value())
    {
        content = std::move(*particleSpecie);
    }
    else
	{
		unsigned int level = 0;
		stringstream ss;

		auto r = formula.rbegin();

		for (; r != formula.rend(); r++)
		{
			bool skip = false;
			if (*r == ')')
			{
				if (level == 0) skip = true;
				level++;
			}


			if (*r == '(')
			{
				level--;
				if (level == 0) skip = true;
			}


			if (level > 0 && !skip)
				ss << *r;
			else
				break;
		}
		string state = ss.str();
		reverse(state.begin(), state.end());


		unsigned int ionization = 0;

		for (; r != formula.rend(); r++)
		{
			if (*r == '+')
				ionization++;
			else if (*r == '-')
				ionization--;
			else if (*r == '^')
			{
				// Nothing
			}
			else
				break;
		}

		string molecule(r, formula.rend());
		reverse(molecule.begin(), molecule.end());

		content = IonSpecie(Molecule(molecule), ionization, state);
	}
}

Specie& Specie::operator=(const Specie &rhs)
{
	if (rhs.isParticle())
		content = rhs.getParticle();
	else if (rhs.isIon())
		content = rhs.getIon();
	
	return *this;	
}

bool Specie::operator==(const Specie &rhs) const
{
	if (isParticle() && rhs.isParticle())
		return getParticle() == rhs.getParticle();
	else if (isIon() && rhs.isIon())
		return getIon() == rhs.getIon();
	else
		return false;
}

bool  Specie::operator!=(const Specie &rhs) const
{
    return !(rhs == *this);
}

bool Specie::isParticle() const
{
    if (get_if<ParticleSpecie>(&content))
        return true;
    else
        return false;
}

bool Specie::isIon() const
{
    if (get_if<IonSpecie>(&content))
        return true;
    else
        return false;
}

const ParticleSpecie& Specie::getParticle() const
{
   return get<ParticleSpecie>(content);
}

const IonSpecie& Specie::getIon() const
{
    return get<IonSpecie>(content);
}

const std::string Specie::toString(const PrintMode& mode)  const
{
	if (isParticle())
		return getParticle().toString(mode);
	else if (isIon())
		return getIon().toString(mode);
	else
		throw ZCrossError("Internal error: specie is not neither an electron or ion.")
}

QtySiMass Specie::getMass() const
{
	if (isParticle())
		return getParticle().getMass();
	else if (isIon())
		return getIon().getMass();
	else
		throw ZCrossError("Internal error: specie is not neither an electron or ion.")
}

QtySiElectricCharge Specie::getCharge() const
{
	if (isParticle())
		return getParticle().getCharge();
	else if (isIon())
		return getIon().getCharge();
	else
		throw ZCrossError("Internal error: specie is not neither an electron or ion.")
}

int Specie::getIonization() const
{
	if (isParticle())
		return getParticle().getIonization();
	else if (isIon())
		return getIon().getIonization();
	else
		throw ZCrossError("Internal error: specie is not neither an electron or ion.")
}

bool Specie::operator<(const Specie& other) const
{
	 if(isParticle() && other.isParticle())
		return getParticle() < other.getParticle();
	 else if (isIon() && other.isIon())
		return getIon() < other.getIon();
	 else if (isParticle() && other.isIon())
		return true;
	 else
		return false;
}

ostream &dfpe::operator<<(ostream &os, const Specie &specie)
{
    os << specie.toString(PrintMode::BASIC);
    return os;
}



