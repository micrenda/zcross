/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */

#include "zcross/GasMixture.hpp"
using namespace dfpe;
using namespace boost::units;
using namespace std;

void GasMixture::addComponent(const string &molecule, const QtySiDensity &numberDensity)
{
    addComponent(Molecule(molecule), numberDensity);
}

void GasMixture::addComponent(const string &molecule, const QtySiMolarDensity &molarDensity)
{
    addComponent(Molecule(molecule), molarDensity);
}

void GasMixture::addComponent(const string &molecule, const QtySiMassDensity &massDensity)
{
    addComponent(Molecule(molecule), massDensity);
}

void GasMixture::addComponent(const Molecule &molecule, const QtySiDensity &numberDensity)
{
    numberDensities[molecule] = numberDensity;
}

void GasMixture::addComponent(const Molecule &molecule,
                              const QtySiMolarDensity &molarDensity)
{
    numberDensities[molecule] = molarDensity * si::constants::codata::N_A;
}

void GasMixture::addComponent(const Molecule &molecule,
                              const QtySiMassDensity &massDensity)
{
    numberDensities[molecule] = massDensity / molecule.getMass();
}

QtySiMassDensity GasMixture::getComponentMassDensity(const Molecule &molecule) const
{
    return numberDensities.at(molecule) * molecule.getMass();
}

QtySiDensity GasMixture::getComponentDensity(const Molecule &molecule) const
{
    return numberDensities.at(molecule);
}

QtySiMolarDensity GasMixture::getComponentMolarDensity(const Molecule &molecule) const
{
    return numberDensities.at(molecule) / si::constants::codata::N_A;
}

unsigned int GasMixture::size() const
{
    return numberDensities.size();
}

bool GasMixture::empty() const
{
    return numberDensities.empty();
}

vector<Molecule> GasMixture::getComponents() const
{
    vector<Molecule> results;

    for (auto pair: numberDensities)
        results.push_back(pair.first);

    return results;
}

void GasMixture::clear()
{
    numberDensities.clear();
}

