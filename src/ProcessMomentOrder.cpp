
#include <string>
#include "zcross/ProcessMomentOrder.hpp"
#include "zcross/ZCrossError.hpp"

using namespace std;

namespace dfpe
{
    const std::string ProcessMomentOrderUtil::getName(int type)
    {
		return  to_string(type);
    }
    
    const std::string ProcessMomentOrderUtil::getDesc(int type)
    {
        switch(type)
        {
            case 0:
                return "(el)";
            case 1:
                return "(mt)";
            case 2:
                return "(vs)";
            default:
				return  "";
        }
    }
    
    int	ProcessMomentOrderUtil::getValue(const ProcessMomentOrderType& type)
    {
        switch(type)
        {
            case ProcessMomentOrderType::EL:
                return 0;
            case ProcessMomentOrderType::MT:
                return 1;
            case ProcessMomentOrderType::VS:
                return 2;
        }

        throw ZCrossError("Unknown process moment order type. Contact developers.");
    }

    int ProcessMomentOrderUtil::parse(const string& s)
    {
        if (s == "el")
            return 0;
        else if (s == "mt")
            return 1;
        else if (s == "vs")
            return 2;
        else
        {
            try
            {
                return stoi(s);
            }
            catch (const invalid_argument& e)
            {
                throw ZCrossError("Unable to parse moment order: " + s);
            }
        }
    }
}


