#include <boost/units/systems/si/prefixes.hpp>
#include <boost/units/systems/si.hpp>
#include <boost/units/quantity.hpp>
#include <boost/units/io.hpp>

#include "zcross/ZCrossError.hpp"
#include "zcross/IntScatteringTable.hpp"
#include "zcross/ZCrossTypes.hpp"

#include <iostream>

using namespace std;
using namespace boost::units;
using namespace dfpe;



void IntScatteringTable::setTable(const std::map<QtySiEnergy, QtySiArea>& table)
{
    IntScatteringTable::table = table;

    if (table.size() >= 4)
    {
        size_t fitDistance = 2; // min(max((int)(table.size() * 0.1), 2), 10);

        const QtySiEnergy& unitEnergy = ZCrossTypes::electronvoltEnergy;
        const QtySiArea    unitArea(1. * si::meter * si::meter);

        // We use a fit of type: y = a x^k (which, in log scale is equivalet to a straight line).
        // It seems to give good results because most cross section, seem to go as a straight line when reaching the energy cs limits

        fitLowerK = QtySiDimensionless();
        fitLowerA = QtySiArea();

        auto lowerP1 = table.begin();
        auto lowerP2 = next(lowerP1, fitDistance);

        if (lowerP1->first == QtySiEnergy())
        {
            // Skip the first point it energy is 0 eV
            lowerP1++;
            lowerP2++;
        }

        if (lowerP1->first >= QtySiEnergy() && lowerP1->second > QtySiArea() &&
            lowerP2->first > QtySiEnergy() && lowerP2->second > QtySiArea())
        {
            QtySiDimensionless logP1X = log(lowerP1->first  / unitEnergy);
            QtySiDimensionless logP1Y = log(lowerP1->second / unitArea);
            QtySiDimensionless logP2X = log(lowerP2->first  / unitEnergy);
            QtySiDimensionless logP2Y = log(lowerP2->second / unitArea);

            fitLowerK = (logP2Y - logP1Y) / (logP2X - logP1X);
            fitLowerA = exp(logP2Y - fitLowerK * logP2X) * unitArea;
        }

        fitUpperK = QtySiDimensionless();
        fitUpperA = QtySiArea();

        auto upperP2 = prev(table.end());
        auto upperP1 = prev(upperP2, fitDistance);


        if (upperP1->first > QtySiEnergy() && upperP1->second > QtySiArea() &&
            upperP2->first > QtySiEnergy() && upperP2->second > QtySiArea())
        {
            QtySiDimensionless logP1X = log(upperP1->first  / unitEnergy);
            QtySiDimensionless logP1Y = log(upperP1->second / unitArea);
            QtySiDimensionless logP2X = log(upperP2->first  / unitEnergy);
            QtySiDimensionless logP2Y = log(upperP2->second / unitArea);

            fitUpperK = (logP2Y - logP1Y) / (logP2X - logP1X);
            fitUpperA = exp(logP2Y - fitUpperK * logP2X) * unitArea;

        }
    }
}

unsigned int IntScatteringTable::getSize() const
{
	return table.size();
}

QtySiEnergy IntScatteringTable::getEnergyLowerBound() const
{
	return table.begin()->first;
}

QtySiEnergy IntScatteringTable::getEnergyUpperBound() const
{
	return table.rbegin()->first;
}

QtySiEnergy IntScatteringTable::getEnergyThreshold() const
{
   if (!energyThreshold.has_value())
   {
       if (process.hasParameter<QtySiEnergy>("E"))
           energyThreshold = process.getParameter<QtySiEnergy>("E");
      else
          energyThreshold = QtySiEnergy();
   }
   //assert(energyThreshold.has_value());

   return energyThreshold.value();
}

string IntScatteringTable::getTableContent(const PrintMode& mode) const
{
    stringstream ss;

    for (auto item: table)
    {
        ss << setw(10) << (item.first / ZCrossTypes::electronvoltEnergy).value() << " eV : " << setw(10) << (item.second / QtySiArea(1. * si::centi * si::meter * si::centi * si::meter)).value();

        if (mode == PrintMode::PRETTY)
            ss << " cm²";
        else
            ss << " cm^2";

        ss << endl;
    }

    return ss.str();
}

TableType IntScatteringTable::getProcessTableType() const
{
    return TableType::INTEGRAL;
}



QtySiArea IntScatteringTable::getInterpoledValue(
	const QtySiEnergy& energy, 
	const OutOfTableMode& lowerBoundaryMode, 
	const OutOfTableMode& upperBoundaryMode) const
{
	
	
	//throw logic_error("Not-implemented, yet!"); // And may be it will never be implemented.
	// Here happens the GPU magic's
	
	/* Here we get the interpoled value for a specific table. We load the data on ZCross::loadXml(...)
	 * 
	 * My main problem is how we can identity this IntScatteringTable in your GPU code (exists the methods IntScatteringTable::getId()
	 * but it returns an human readable string (something like: Biagi/CO2/1 ) and I don't think it is very useful for you to search   
	 * in the GPU memory which table to look for.
	 * 
	 * If you think it is useful, I may add an unique integer ID for each IntScatteringTable instance.
	 * 
	 * Here you look in your cross section tables for the cross section for a given energy.
	 * 
	 * Let imagine the table something like this:
	 * 
	 * Energy (eV) | Cross Section (ym2)              
	 * ____________|______________
	 *          1  |           0.5
	 *          1.5|           1
	 *          2  |           5
	 *          5  |           6
	 *         10  |           4
	 *         15  |           3   
	 *       ...   |   ...
	 * 
	 * 
	 * We know that energy is ordered (cross section is not). If we call this function with energy E = 7eV we can get
	 * the interpoled value searching for the lowest energy bound E0 = 5 eV, E1 = 10 eV -> C0 = 6 ym2, C1 = 4 ym2.
	 * 
	 * The interpoled value will be:
	 * C = C0 + (C1 - C0) * (E - E0) / (E1 - E0) = 5.2 ym2
	 * 
	 * The searching can be speeded up by a parallel search or a binary lookup. This code is execute very often.
	 * 
	 * The flags OutOfTableMode control what to do if E is outside the table boundaries (it is used)
	 * 
	 * OutOfTableMode::ZERO_VALUE       -> Return 0
	 * OutOfTableMode::BOUNDARY_VALUE   -> Return first or last value
	 * OutOfTableMode::LAUNCH_EXCEPTION -> Launch exception
	 * 
	 * Before to pass the value back to CPU code, remember to convert back to boost units:
	 * 									 
	 * QtySiArea   ym2(1e-24 * si::meter * si::meter); // yocto m^2
	 * QtySiEnergy ev(1. * si::volt  * si::constants::codata::e); // eV
	 * 
	 * This is all :)
	 */


    //if (lastUsedIt.has_value())
    //{
    //    std::map<QtySiEnergy, QtySiArea>::const_iterator itLast = lastUsedIt.value();
    //    if (itLast != table.begin() && itLast != table.end())
    //    {
    //        QtySiEnergy energyUpper = itLast->first;
    //        QtySiArea   areaUpper   = itLast->second;

    //        QtySiEnergy energyLower = prev(itLast)->first;
    //        QtySiArea areaLower = prev(itLast)->second;

    //        return areaLower + (areaUpper - areaLower) * (energy - energyLower) / (energyUpper - energyLower);
    //    }
    //}

    std::map<QtySiEnergy, QtySiArea>::const_iterator it = table.lower_bound(energy);

    if (!table.empty())
    {
        if (it != table.end())
        {

            //lastUsedIt = it;


            const QtySiEnergy& energyUpper = it->first;
            const QtySiArea&   areaUpper   = it->second;

            if (it != table.begin())
            {
                --it;

                const QtySiEnergy& energyLower = it->first;
                const QtySiArea&   areaLower = it->second;

                return areaLower + (areaUpper - areaLower) * (energy - energyLower) / (energyUpper - energyLower);

            }
            else
            {
				if (lowerBoundaryMode == OutOfTableMode::ZERO_VALUE)
					return QtySiArea();
				else if (lowerBoundaryMode == OutOfTableMode::BOUNDARY_VALUE)
					return table.begin()->second;
                else if (lowerBoundaryMode == OutOfTableMode::FIT_VALUE)
                {
                    if (table.begin()->second == QtySiArea())
                        return QtySiArea();
                    else if (fitLowerK != 0. || fitLowerA != QtySiArea())
                        return fitLowerA * pow(energy / ZCrossTypes::electronvoltEnergy, fitLowerK);
                    else
                    {
                        stringstream ss;
                        ss << "Impossible use the FIT_VALUE mode for the table '" << getId().toString() << "' for energy " << (energy / ZCrossTypes::electronvoltEnergy).value() << " eV: the table contains too few values.";
                        throw ZCrossError(ss.str());
                    }
                }
				else
				{
					stringstream ss;
					ss << "Impossible to get a valid cross section '" << getShortDescription() << "' for energy " << (energy / ZCrossTypes::electronvoltEnergy).value() << " eV: the value is too low. Current table cover energies between " << (table.begin()->first / ZCrossTypes::electronvoltEnergy).value() << " eV and " << (table.rbegin()->first / ZCrossTypes::electronvoltEnergy).value() << "eV.";
					throw ZCrossError(ss.str());
				}
            }
        }
        else
        {
			if (upperBoundaryMode == OutOfTableMode::ZERO_VALUE)
					return QtySiArea();
			else if (upperBoundaryMode == OutOfTableMode::BOUNDARY_VALUE)
				return table.rbegin()->second;
			else if (upperBoundaryMode == OutOfTableMode::FIT_VALUE)
            {
                if (table.rbegin()->second == QtySiArea())
                    return QtySiArea();
                else if (fitUpperK != 0. || fitUpperA != QtySiArea())
			        return fitUpperA * pow(energy / ZCrossTypes::electronvoltEnergy, fitUpperK);
                else
                {
                    stringstream ss;
                    ss << "Impossible use the FIT_VALUE mode for the table '" << getId().toString() << "' for energy " << (energy / ZCrossTypes::electronvoltEnergy).value() << " eV: the table contains too few values.";
                    throw ZCrossError(ss.str());
                }
            }
			else
			{
				stringstream ss;
				ss << "Impossible to get a valid cross section '" << getShortDescription() << "' for energy " << (energy / ZCrossTypes::electronvoltEnergy).value() << " eV: the value is too high. Current table cover energies between " << (table.begin()->first /ZCrossTypes::electronvoltEnergy).value() << " eV and " << (table.rbegin()->first / ZCrossTypes::electronvoltEnergy).value() << " eV.";
				throw ZCrossError(ss.str());
			}
        }
    }
    else
    {
        stringstream ss;
        ss << "Impossible to get a valid cross section '" << getShortDescription() << "' for energy " << (energy / ZCrossTypes::electronvoltEnergy).value() << " eV: the current table is empty.";
        throw ZCrossError(ss.str());
    }
}


QtySiArea IntScatteringTable::getSpan(const QtySiEnergy& lower, const QtySiEnergy& upper) const
{
    QtySiEnergy eV = ZCrossTypes::electronvoltEnergy;
    QtySiArea result;

    std::map<QtySiEnergy, QtySiArea>::const_iterator it = table.lower_bound(lower);

    if (it->first != lower && it != table.begin())
    {
        QtySiEnergy e1, e2, e3;
        QtySiArea   v1, v2, v3;

        e1 = prev(it)->first;
        e2 = lower;
        e3 = it->first;

        v1 = prev(it)->second;
        v3 = it->second;
        v2 = v1 + (v3 - v1) * (e3 - e2) / (e3 - e1);


        result += (v3 + v2) / 2. * (log10(e3 / eV) - log10(e2/eV));
    }

    while(true)
    {
        QtySiEnergy e1, e3;
        QtySiArea   v1, v3;

        e1 = it->first;
        v1 = it->second;
        it = next(it);

        e3 = it->first;
        v3 = it->second;

        if (e3 <= upper)
        {
            result += (v3 + v1) / 2. * (log10(e3 / eV) - log10(e1 / eV));

            if (e3 == upper)
                break;
        }
        else
        {

            QtySiEnergy e1, e2, e3;
            QtySiArea   v1, v2, v3;

            e1 = prev(it)->first;
            e2 = lower;
            e3 = it->first;

            v1 = prev(it)->second;
            v3 = it->second;
            v2 = v1 + (v3 - v1) * (e3 - e2) / (e3 - e1);

            result += (v2 + v1) / 2. * (log10(e2 / eV) - log10(e1/eV));

            break;
        }
    }

    return result;
}