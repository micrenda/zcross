#include <boost/regex.hpp>
#include "zcross/Utils.hpp"

using namespace std;
using namespace dfpe;
using namespace boost;

const string Utils::getScriptedNumber(int number, bool superscript, bool forcePlus)
{

    const vector<string> digitsSuper = {u8"⁰",u8"¹",u8"²",u8"³",u8"⁴",u8"⁵",u8"⁶",u8"⁷",u8"⁸",u8"⁹"};
    const vector<string> digitsSub   = {u8"₀",u8"₁",u8"₂",u8"₃",u8"₄",u8"₅",u8"₆",u8"₇",u8"₈",u8"₉"};

    string s;

    if (number < 0)
    {
        if (superscript)
            s += u8"⁻";
        else
            s += u8"₋";
    }
    else if (number > 0 and forcePlus)
    {
        if (superscript)
            s += u8"⁺";
        else
            s += u8"₊";
    }

    while (number > 0)
    {
        if (superscript)
            s = digitsSuper[number % 10] + s;
        else
            s = digitsSub[number % 10] + s;

        number /= 10;
    }

    return s;
}

const std::string Utils::formatElementPretty(const boost::smatch &what)
{
    return what[1] + getScriptedNumber(stoi(what[2]), false);
}

const std::string Utils::formatElementLatex(const boost::smatch &what)
{
    return what[1] + what[2];
}

const regex Utils::elementRegex         = regex( "([A-Z][a-z]{0,2})([0-9]*)");
const regex Utils::isomerRegex          = regex( "^([a-z0-9])-");
