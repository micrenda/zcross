
#include <string>
#include "zcross/ProcessDirectionType.hpp"
#include "zcross/ZCrossError.hpp"

using namespace std;

namespace dfpe
{
	const std::string ProcessDirectionUtil::getNameDefault(const ProcessDirectionType& type, const std::string& defaultValue)
	{
		if (type == ProcessDirectionType::UNKNOWN)
			return defaultValue;
		else
			return getName(type);
	}
	 
    const std::string ProcessDirectionUtil::getName(const ProcessDirectionType& type)
    {
        switch(type)
        {
            case ProcessDirectionType::ISOTROPIC:
                return "isotropic";
            case ProcessDirectionType::ANISOTROPIC:
                return "anisotropic";
            case ProcessDirectionType::BACKSCATTERING:
                return "backscattering";
            case ProcessDirectionType::UNKNOWN:
                return "unknown";
        }

        throw ZCrossError("Unknown process direction type. Contact developers.");
    }
    
    const std::string ProcessDirectionUtil::getDesc(const ProcessDirectionType& type)
    {
        switch(type)
        {
            case ProcessDirectionType::ISOTROPIC:
                return "Isotropic";
            case ProcessDirectionType::ANISOTROPIC:
                return "Anisotropic";
            case ProcessDirectionType::BACKSCATTERING:
                return "Back-scattering";
            case ProcessDirectionType::UNKNOWN:
                return "Unknown";
        }

        throw ZCrossError("Unknown process direction type. Contact developers.");
    }

    ProcessDirectionType ProcessDirectionUtil::parse(const string& s)
    {
        if (s == "isotropic")
            return ProcessDirectionType::ISOTROPIC;
        else if (s == "anisotropic")
            return ProcessDirectionType::ANISOTROPIC;
        else if (s == "backscattering")
            return ProcessDirectionType::BACKSCATTERING;
        else if (s == "")
            return ProcessDirectionType::UNKNOWN;
        else
            throw ZCrossError("Unknown process direction type: '" + s + "'. Contact developers.");
    }
}


