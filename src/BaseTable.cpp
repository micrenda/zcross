#include <ostream>
#include <boost/units/systems/si/codata/electromagnetic_constants.hpp>
#include <boost/algorithm/string/case_conv.hpp>
#include "zcross/ZCrossError.hpp"
#include "zcross/BaseTable.hpp"

using namespace dfpe;
using namespace std;
using namespace boost::units;




void BaseTable::validate() const
{
    const Reaction& reaction = process.getReaction();

    if (reaction.hasProducts())
    {
        if (!reaction.hasMassConservation())
            throw ZCrossError("The table '" + getId().toString() + "' is invalid because there is no mass conservation: " + reaction.toString(PrintMode::BASIC));
        if (!reaction.hasChargeConservation())
            throw ZCrossError("The table '" + getId().toString() + "' is invalid because there is no charge conservation: " + reaction.toString(PrintMode::BASIC));


        // Check electron differences for different inelastic reactions
        if (process.getCollisionType() == ProcessCollisionType::INELASTIC)
        {
            const Reactants& reactants = reaction.getReactants();

            for (const Products& product: reaction.getProducts())
            {
                if (process.getCollisionInelasticType() == ProcessCollisionInelasticType::IONIZATION)
                {
                    if (!(reactants.getElectronsCount() < product.getElectronsCount()))
                        throw ZCrossError("The table '" + getId().toString() +
                                          "' is invalid because in the IONIZATION process is not created a new electron: " +
                                          reaction.toString(PrintMode::BASIC));
                }
                else if (process.getCollisionInelasticType() == ProcessCollisionInelasticType::ATTACHMENT)
                {
                    if (!(reactants.getElectronsCount() > product.getElectronsCount()))
                        throw ZCrossError("The table '" + getId().toString() +
                                          "' is invalid because in the ATTACHMENT process is not destroyed any electron: " +
                                          reaction.toString(PrintMode::BASIC));
                }
                else
                {
                    if (reactants.getElectronsCount() != product.getElectronsCount())
                    {
                        throw ZCrossError("The table '" + getId().toString() + "' is invalid because in the process the number of electrons is not preserved: " +
                                          reaction.toString(PrintMode::BASIC));
                    }
                }
            }
        }
    }
}

std::string BaseTable::getShortDescription() const
{
    stringstream ss;
    ss << TableTypeUtil::getName(getProcessTableType()) << " " << getProcess().getShortDescription() << " " << "table";
    return ss.str();
}

const TableId& BaseTable::getId() const
{
    return tableId;
}


bool BaseTable::operator==(const BaseTable &rhs) const
{
    return tableId == rhs.tableId;
}

bool BaseTable::operator!=(const BaseTable &rhs) const
{
    return !(rhs == *this);
}

const std::string BaseTable::toString(const PrintMode &printMode) const
{
    return tableId.toString(printMode) + " (" + to_string(getSize()) + " entries)";
}

bool BaseTable::operator<(const BaseTable &rhs) const
{
    return tableId < rhs.tableId;
}

bool BaseTable::operator>(const BaseTable &rhs) const
{
    return rhs < *this;
}

bool BaseTable::operator<=(const BaseTable &rhs) const
{
    return !(rhs < *this);
}

bool BaseTable::operator>=(const BaseTable &rhs) const
{
    return !(*this < rhs);
}



/*
bool BaseTable::operator<(const BaseTable &rhs) const {
    if (databaseId < rhs.databaseId)
        return true;
    if (rhs.databaseId < databaseId)
        return false;
    if (databaseName < rhs.databaseName)
        return true;
    if (rhs.databaseName < databaseName)
        return false;
    if (groupId < rhs.groupId)
        return true;
    if (rhs.groupId < groupId)
        return false;
    if (groupDescription < rhs.groupDescription)
        return true;
    if (rhs.groupDescription < groupDescription)
        return false;
    if (processClass < rhs.processClass)
        return true;
    if (rhs.processClass < processClass)
        return false;
    if (processType < rhs.processType)
        return true;
    if (rhs.processType < processType)
        return false;
    return processReaction < rhs.processReaction;
}

bool BaseTable::operator>(const BaseTable &rhs) const {
    return rhs < *this;
}

bool BaseTable::operator<=(const BaseTable &rhs) const {
    return !(rhs < *this);
}

bool BaseTable::operator>=(const BaseTable &rhs) const {
    return !(*this < rhs);
}
*/

