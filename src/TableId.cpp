#include "zcross/TableId.hpp"
#include <sstream>
#include <iostream>
using namespace dfpe;
using namespace std;

string TableId::toString(const PrintMode&) const
{
    stringstream ss;
    if (!isDerived())
    {
        ss << databaseId << "/" << groupId << "/" << to_string(processId);
    }
    else
    {
        ss << label << ": (";

        bool first = true;
        for (const TableId &parent: parents)
        {
            if (!first)
                ss << " + ";
            else
                first = false;

            ss << parent.toString();
        }
        ss << ")";
    }
    return ss.str();
}

bool TableId::operator==(const TableId &rhs) const
{
    return databaseId == rhs.databaseId &&
           groupId    == rhs.groupId    &&
           processId  == rhs.processId  &&
           label      == rhs.label      &&
           parents    == rhs.parents;
}

bool TableId::operator!=(const TableId &rhs) const
{
    return !(rhs == *this);
}

bool TableId::operator<(const TableId &rhs) const
{
    // Derived < Not Derived
    if (isDerived() != rhs.isDerived())
    {
        return isDerived();
    }
    else if (isDerived()) // both are derived
    {
        if (label != rhs.label)
            return label < rhs.label;

        size_t n = min(parents.size(), rhs.parents.size());

        for (size_t i = 0; i < n; i++)
        {
            if (parents[i] != rhs.parents[i])
                return parents[i] != rhs.parents[i];
        }

        return parents.size() < rhs.parents.size();
    }
    else // both are not derived
    {
        if (databaseId != rhs.databaseId)
            return databaseId < rhs.databaseId;

        if (groupId != rhs.groupId)
            return groupId < rhs.groupId;

        return processId < rhs.processId;
    }
}

bool TableId::operator>(const TableId &rhs) const
{
    return rhs < *this;
}

bool TableId::operator<=(const TableId &rhs) const
{
    return !(rhs < *this);
}

bool TableId::operator>=(const TableId &rhs) const
{
    return !(*this < rhs);
}

ostream& dfpe::operator<<(ostream &os, const TableId &other)
{
    os << other.toString(PrintMode::BASIC);
    return os;
}
