#include <zcross/ZCrossError.hpp>
#include <filesystem>
#include <boost/iterator/iterator_facade.hpp>
#include <boost/tokenizer.hpp>
#include <boost/units/systems/detail/constants.hpp>
#include <boost/units/systems/si/codata/physico-chemical_constants.hpp>
#include <boost/units/systems/si/mass.hpp>
#include <fstream>
#include <stdexcept>
#include <string>

#include "zcross/PeriodicTable.hpp"
#include "ZCrossConfig.hpp"

using namespace std;
using namespace boost;
using namespace boost::units;
using namespace std::filesystem;
using namespace dfpe;

PeriodicTable::PeriodicTable()
{
	tablePath = path("periodic.csv");

	if (!exists(tablePath))
		tablePath = path(ZCROSS_INSTALL_PREFIX "/share/zcross/periodic.csv");
    
    if (!exists(tablePath))
		throw ZCrossError("Unable to load the periodic table: " + tablePath.string())
        
	isotopesPath = path("isotopes.csv");

    if (!exists(isotopesPath))
        isotopesPath = path(ZCROSS_INSTALL_PREFIX "/share/zcross/isotopes.csv");
        
    if (!exists(isotopesPath))
		throw ZCrossError("Unable to load the isotopes table: " + isotopesPath.string())
}

void PeriodicTable::load(std::filesystem::path filePath)
{
	ifstream file(filePath);
	
    std::string line;

    bool first = true;
    while(getline(file,line))
    {
        if (first) // Skip header
        {
            first = false;
            continue;
        }
        
		unsigned int z=0;
		std::string  symbol;
		std::string  name;
		QtySiMass mass;
    
        unsigned int c = 0;
        char_separator<char> sep(",");
        tokenizer<char_separator<char>> tok(line, sep);
        for(tokenizer<char_separator<char>>::iterator beg=tok.begin(); beg!=tok.end(); ++beg)
        {
            switch (c)
            {
                case 0:
                    z = (unsigned int) stoi(*beg);
                    break;
                case 1:
                    name = *beg;
                    break;
                case 2:
                    symbol = *beg;
                    break;
                case 3:
                    mass = QtySiMass(stod(*beg) * si::constants::codata::m_u);
                    break;
                default:
                    break;
            }

            c++;
        }
        
        
        elements[symbol] = Element(z, symbol, name, mass);

    }
    file.close();
}

const PeriodicTable& PeriodicTable::getInstance()
{
    if (PeriodicTable::instance.elements.empty())
    {
        PeriodicTable::instance.load(PeriodicTable::instance.tablePath);
        PeriodicTable::instance.load(PeriodicTable::instance.isotopesPath);
        
        if (PeriodicTable::instance.elements.empty())
			throw ZCrossError("Unable to load elements symbols.")
    }

    return PeriodicTable::instance;
}

const Element& PeriodicTable::parseElement(const std::string& symbol) const
{
    try
    {
        return elements.at(symbol);
    }
    catch (std::out_of_range& e)
    {
        throw ZCrossError("Unable to find the '" + symbol + "' element")
    }


}

PeriodicTable PeriodicTable::instance;
