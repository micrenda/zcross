
#include <string>
#include "zcross/ProcessCollisionInelasticType.hpp"
#include "zcross/ZCrossError.hpp"

using namespace std;

namespace dfpe
{
    const std::string ProcessCollisionInelasticUtil::getName(const ProcessCollisionInelasticType& type)
    {
        switch(type)
        {
            case ProcessCollisionInelasticType::EXCITATION_ELE:
                return "excitation_ele";
            case ProcessCollisionInelasticType::EXCITATION_VIB:
                return "excitation_vib";
            case ProcessCollisionInelasticType::EXCITATION_ROT:
                return "excitation_rot";
            case ProcessCollisionInelasticType::IONIZATION:
                return "ionization";
            case ProcessCollisionInelasticType::ATTACHMENT:
                return "attachment";
            case ProcessCollisionInelasticType::NEUTRAL:
                return "neutral";
            case ProcessCollisionInelasticType::NONE:
                return "";
        }

        throw ZCrossError("Unknown process inelastic type. Contact developers.");
    }
    
    const std::string ProcessCollisionInelasticUtil::getDesc(const ProcessCollisionInelasticType& type)
    {
        switch(type)
        {
            case ProcessCollisionInelasticType::EXCITATION_ELE:
                return "Excitation (electronic)";
            case ProcessCollisionInelasticType::EXCITATION_VIB:
                return "Excitation (vibrational)";
            case ProcessCollisionInelasticType::EXCITATION_ROT:
                return "Excitation (rotational)";
            case ProcessCollisionInelasticType::IONIZATION:
                return "Ionization";
            case ProcessCollisionInelasticType::ATTACHMENT:
                return "Attachment";
            case ProcessCollisionInelasticType::NEUTRAL:
                return "Neutral";
            case ProcessCollisionInelasticType::NONE:
                return "";
        }

        throw ZCrossError("Unknown process inelastic type. Contact developers.");
    }

    ProcessCollisionInelasticType ProcessCollisionInelasticUtil::parse(const string& s)
    {
        if (s == "excitation_ele")
            return ProcessCollisionInelasticType::EXCITATION_ELE;
        else if (s == "excitation_vib")
            return ProcessCollisionInelasticType::EXCITATION_VIB;
        else if (s == "excitation_rot")
            return ProcessCollisionInelasticType::EXCITATION_ROT;
        else if (s == "ionization")
            return ProcessCollisionInelasticType::IONIZATION;
        else if (s == "attachment")
            return ProcessCollisionInelasticType::ATTACHMENT;
        else if (s == "neutral")
            return ProcessCollisionInelasticType::NEUTRAL;
        else if (s == "")
            return ProcessCollisionInelasticType::NONE;
        else
            throw ZCrossError("Unknown process inelastic type: '" + s + "'. Contact developers.");
    }
}


