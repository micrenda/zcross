#include <boost/units/quantity.hpp>
#include <boost/units/systems/si.hpp>
#include <boost/units/systems/si/codata_constants.hpp>
#include "zcross/ZCrossTypes.hpp"

using namespace dfpe;
using namespace boost::units;

QtySiEnergy ZCrossTypes::electronvoltEnergy(1. * si::volt * si::constants::codata::e);
