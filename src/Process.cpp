#include <zcross/TableType.hpp>
#include <sstream>
#include "zcross/Process.hpp"
#include "zcross/MoleculeComponent.hpp"

using namespace dfpe;
using namespace std;






std::string Process::getShortDescription() const
{
    vector<string> tokens;

    switch(getCollisionType())
    {
        case ProcessCollisionType::TOTAL:
        case ProcessCollisionType::ELASTIC:
        case ProcessCollisionType::SUPERELASTIC:
            tokens.push_back(ProcessCollisionUtil::getName(getCollisionType()));
            break;
        default:
            break;
    }

    if (isDissociativeFlag())
    {
        tokens.push_back("dissociative");
    }


    switch (getCollisionInelasticType())
    {
        case ProcessCollisionInelasticType::EXCITATION_ELE:
            tokens.push_back("excitation (ele)");
            break;
        case ProcessCollisionInelasticType::EXCITATION_VIB:
            tokens.push_back("excitation (vib)");
            break;
        case ProcessCollisionInelasticType::EXCITATION_ROT:
            tokens.push_back("excitation (rot)");
            break;
        case ProcessCollisionInelasticType::IONIZATION:
            tokens.push_back("ionization");
            break;
        case ProcessCollisionInelasticType::ATTACHMENT:
            tokens.push_back("attachment");
            break;
        case ProcessCollisionInelasticType::NEUTRAL:
            tokens.push_back("neutre");
            break;
        default:
            break;
    }


    switch(getDirectionType())
    {
        case ProcessDirectionType::ISOTROPIC:
            tokens.push_back("isotropic");
            break;
        case ProcessDirectionType::ANISOTROPIC:
            tokens.push_back("anisotropic");
            break;
        case ProcessDirectionType::BACKSCATTERING:
            tokens.push_back("backscattering");
            break;

        default:
            break;
    }

    tokens.push_back("cross-section");

    if (getCollisionType() == ProcessCollisionType::ELASTIC)
	    tokens.push_back(ProcessMomentOrderUtil::getDesc(getMomentOrder()));
	
    stringstream ss;

    bool first = true;
    for (string token: tokens)
    {
        if (!first)
            ss << " ";
        ss << token;

        first = false;
    }

    return ss.str();
}

std::string Process::getShortestDescription() const
{
    stringstream ss;
    if (getCollisionType() == ProcessCollisionType::ELASTIC)
        ss << "ela";
    else if (getCollisionType() == ProcessCollisionType::INELASTIC)
    {

        switch (getCollisionInelasticType())
        {
            case ProcessCollisionInelasticType::EXCITATION_ELE:
            case ProcessCollisionInelasticType::EXCITATION_VIB:
            case ProcessCollisionInelasticType::EXCITATION_ROT:
                ss << "exc";
                break;
            case ProcessCollisionInelasticType::ATTACHMENT:
                ss << "att";
                break;
            case ProcessCollisionInelasticType::IONIZATION:
                ss << "ion";
                break;
            case ProcessCollisionInelasticType::NEUTRAL:
                ss << "neu";
                break;
            case ProcessCollisionInelasticType::NONE:
                ss << "";
                break;
        }
    }
    else if (getCollisionType() == ProcessCollisionType::SUPERELASTIC)
        ss << "sup";
    else if (getCollisionType() == ProcessCollisionType::TOTAL)
        ss << "tot";
    else
        ss << "???";

    return ss.str();
}


std::string Process::getCompactDescription() const
{
    stringstream ss;
    if (getCollisionType() == ProcessCollisionType::ELASTIC)
        ss << "elastic";
    else if (getCollisionType() == ProcessCollisionType::INELASTIC)
    {

        switch (getCollisionInelasticType())
        {
            case ProcessCollisionInelasticType::EXCITATION_ELE:
                ss << "excitation (ele)";
                break;
            case ProcessCollisionInelasticType::EXCITATION_VIB:
                ss << "excitation (vib)";
                break;
            case ProcessCollisionInelasticType::EXCITATION_ROT:
                ss << "excitation (rot)";
                break;
            case ProcessCollisionInelasticType::ATTACHMENT:
                ss << "attachment";
                break;
            case ProcessCollisionInelasticType::IONIZATION:
                ss << "ionization";
                break;
            case ProcessCollisionInelasticType::NEUTRAL:
                ss << "neutral";
                break;
            case ProcessCollisionInelasticType::NONE:
                ss << "";
                break;
        }
    }
    else if (getCollisionType() == ProcessCollisionType::SUPERELASTIC)
        ss << "superelastic";
    else if (getCollisionType() == ProcessCollisionType::TOTAL)
        ss << "total";
    else
        ss << "???";

    return ss.str();
}


std::string Process::getVeryShortDescription() const
{
    stringstream ss;
    if (getCollisionType() == ProcessCollisionType::ELASTIC)
        ss << "ela";
    else if (getCollisionType() == ProcessCollisionType::INELASTIC)
    {

        switch (getCollisionInelasticType())
        {
            case ProcessCollisionInelasticType::EXCITATION_ELE:
                ss << "exc(ele)";
                break;
            case ProcessCollisionInelasticType::EXCITATION_VIB:
                ss << "exc(vib)";
                break;
            case ProcessCollisionInelasticType::EXCITATION_ROT:
                ss << "exc(rot)";
                break;
            case ProcessCollisionInelasticType::ATTACHMENT:
                ss << "att";
                break;
            case ProcessCollisionInelasticType::IONIZATION:
                ss << "ion";
                break;
            case ProcessCollisionInelasticType::NEUTRAL:
                ss << "neu";
                break;
            case ProcessCollisionInelasticType::NONE:
                ss << "";
                break;
        }
    }
    else if (getCollisionType() == ProcessCollisionType::SUPERELASTIC)
        ss << "sup";
    else if (getCollisionType() == ProcessCollisionType::TOTAL)
        ss << "tot";
    else
        ss << "???";

    return ss.str();
}
