#include <boost/operators.hpp>
#include <boost/operators.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/units/quantity.hpp>
#include <boost/units/systems/detail/constants.hpp>
#include <boost/units/systems/si.hpp>
#include <boost/units/systems/si/prefixes.hpp>
#include <boost/units/systems/angle/degrees.hpp>
#include <boost/units/systems/si/codata/electromagnetic_constants.hpp>
#include <boost/units/systems/si/codata/universal_constants.hpp>
#include <boost/units/systems/si/codata/atomic-nuclear_constants.hpp>
#include <boost/algorithm/string/case_conv.hpp>
#include <algorithm>
#include <chrono>
#include <ctime>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <zcross/ElectronSpecie.hpp>


#include "ZCrossConfig.hpp"
#include "zcross/ZCross.hpp"
#include "zcross/Specie.hpp"
#include "zcross/Specie.hpp"
#include "zcross/ParticleSpecie.hpp"
#include "zcross/Hashes.hpp"
#include "zcross/Hashes.hpp"
#include "zcross/IonSpecie.hpp"
#include "zcross/Molecule.hpp"
#include "zcross/Reaction.hpp"
#include "zcross/Reaction.hpp"
#include "zcross/IntScatteringTable.hpp"
#include "zcross/ZCrossError.hpp"

using namespace dfpe;
using namespace std::filesystem;
using namespace boost::units;
using namespace boost::algorithm;
using namespace boost;
using namespace std;
namespace pt = boost::property_tree;

ZCross::ZCross()
{
    initDatabasesMap();
}

ZCross::~ZCross()
{
}

vector<path> ZCross::getDataPaths() const
{
	vector<path> base;
	
	char* dataPathEnv = getenv("ZCROSS_DATA");
    if (dataPathEnv!=NULL)
    {
        string s(dataPathEnv);

        istringstream tokenStream(s);
        string token;
        while (std::getline(tokenStream, token, ':'))
            base.push_back(path(token));
    }
    else if (exists(path("opt") / path("zcross_data")) && is_directory(path("opt") / path("zcross_data")))
		base.push_back(path(path("opt") / path("zcross_data")));
	else
		throw ZCrossError("Unable to find ZCross data file: define ZCROSS_DATA env variable.")

	for (const auto& b: base)
	{
	    if (!exists(b) || !is_directory(b))
	        throw ZCrossError("Unable to access the directory '" +  b.string() + "' defined in ZCROSS_DATA env variable.");
	}

    
    return base;
}

void ZCross::initDatabasesMap()
{
    databasesMap.clear();
    databasesLoaded.clear();

	for (path& base : getDataPaths())
	{
	    recursive_directory_iterator it(base);
	    recursive_directory_iterator endit;

	    while (it != endit)
	    {
	        if (is_regular_file(*it) && it->path().extension().string() == ".xml")
	        {
	            pt::ptree docXml;
	            pt::read_xml(it->path().string(), docXml, boost::property_tree::xml_parser::trim_whitespace | boost::property_tree::xml_parser::no_comments);

	            for (pt::ptree::value_type &databasesXml : docXml.get_child("zcross"))
	            {
	                if (databasesXml.first == "database")
	                {
	                    pt::ptree &databaseXml = databasesXml.second;

	                    string databaseId = databaseXml.get<string>("<xmlattr>.id");

                        if (databasesMap.find(databaseId) == databasesMap.end())
                            databasesMap[databaseId] = it->path();
                        else
                            throw ZCrossError("Multiple XML's were found with the same database id: " + databaseId + ". Check the content of ZCROSS_DATA env variable.");
	                }
	            }
	        }
	        ++it;
	    }
	}
}
vector<path> ZCross::listDatabasesFiles() const
{
    vector<path> results;
	for (auto& base: getDataPaths())
	{
	    recursive_directory_iterator it(base);
	    recursive_directory_iterator endit;

	    while (it != endit)
	    {
	        if (is_regular_file(*it) && it->path().extension().string() == ".xml") results.push_back(it->path());
	        ++it;
	    }
	}
    return results;
}

void ZCross::loadDatabases()
{
    for (path file: listDatabasesFiles())
    {
        loadDatabaseFile(file);
    }
}

void ZCross::loadDatabases(const vector<string>& names)
{
	for (const string& name: names) 
	{
		loadDatabase(name);
	}
}


void ZCross::loadDatabase(const string &name)
{
	string localName = name;
	std::transform(localName.begin(), localName.end(), localName.begin(), ::tolower);
	
    try
    {
        loadDatabaseFile(databasesMap.at(localName));
    }
    catch (const out_of_range&)
    {
        throw ZCrossError("Unable to load database: " + name)
    }
}

void ZCross::loadDatabaseFile(const path &filename)
{
    try
    {
        if (filename.extension().string() == ".xml")
        {
            pt::ptree docXml;
            pt::read_xml(filename.string(), docXml, boost::property_tree::xml_parser::trim_whitespace | boost::property_tree::xml_parser::no_comments);
            loadXml(docXml);
        } else
            throw ZCrossError("Unknown file extension '" + filename.extension().string() + "'");
    }
    catch (const ZCrossError &e)
    {
        throw ZCrossErrorFwd("Error while reading file '" + filename.string() + "'", e);
    }
}

QtySiArea parseArea(double value, string unit)
{
    if (unit == "m2")
        return QtySiArea(value * si::square_meters);
    if (unit == "cm2")
        return QtySiArea(value * si::centi * si::meters * si::centi * si::meters);
    if (unit == "Bohr2")
        return QtySiArea(value * si::constants::codata::a_0 * si::constants::codata::a_0);

    throw ZCrossError("Unable to parse area unit: " + unit);
}

QtySiEnergy parseEnergy(double value, string unit)
{
    if (unit == "eV")
        return QtySiEnergy(value * si::volts * si::constants::codata::e);
    if (unit == "Hartee")
        return QtySiEnergy(value * si::constants::codata::m_e * si::constants::codata::c *  si::constants::codata::c * si::constants::codata::alpha * si::constants::codata::alpha);

    throw ZCrossError("Unable to parse energy unit: " + unit);
}

QtySiPlaneAngle parsePlaneAngle(double value, string unit)
{
    if (unit == "deg")
        return QtySiPlaneAngle(value * degree::degrees);

    throw ZCrossError("Unable to parse plane angle unit: " + unit);
}

QtySiAreaPerSolidAngle parseAreaPerSolidAngle(double value, string unit)
{
    if (unit == "cm2/sr")
        return QtySiAreaPerSolidAngle(value * si::centi * si::meter * si::centi * si::meter / si::steradian);
    if (unit == "m2/sr")
        return QtySiAreaPerSolidAngle(value * si::meter * si::meter / si::steradian);

    throw ZCrossError("Unable to parse area per solid angle unit: " + unit);
}

bool ZCross::isDatabaseLoaded(string databaseId)
{
	
	string localDatabaseId = databaseId;
	std::transform(localDatabaseId.begin(), localDatabaseId.end(), localDatabaseId.begin(), ::tolower);
	
    auto dbLoaded = databasesLoaded.find(localDatabaseId);
    if ( dbLoaded != databasesLoaded.end())
    {
        if (dbLoaded->second == true)
            return true;
    }
    return false;
}

vector<int> splitByComma(const std::string s)
{
    vector<int> result;

    std::stringstream ss(s);
    int i;
    while (ss >> i)
    {
        result.push_back(i);

        if (ss.peek() == ',')
            ss.ignore();
    }

    return result;
}

void ZCross::loadXml(boost::property_tree::ptree &rootXml)
{

    for (pt::ptree::value_type &databasesXml : rootXml.get_child("zcross"))
    {
        if (databasesXml.first == "database")
        {
            pt::ptree &databaseXml = databasesXml.second;

            string databaseId          = databaseXml.get<string>("<xmlattr>.id", "");
            string databaseRefs        = databaseXml.get<string>("<xmlattr>.refs", "");

            // Testing if the database is already loaded
            if (isDatabaseLoaded(databaseId))
                continue;


            databasesLoaded[databaseId] = true;

            string databaseName        = databaseXml.get<string>("name", "");
            string databaseUrl         = databaseXml.get<string>("url", "");
            string databaseDescription = databaseXml.get<string>("description", "");




            for (pt::ptree::value_type &referencesXml : databaseXml.get_child("references"))
            {
                if (referencesXml.first == "reference")
                {
                    if (references.find(databaseId) == references.end())
                        references[databaseId] = map<int,Reference>();

                    int referenceId = referencesXml.second.get<int>("<xmlattr>.id");
                    references[databaseId][referenceId] = Reference::parse(referencesXml.second.get_value<string>());
                }
            }

            string databaseContacts = databaseXml.get<string>("contact", "");

            for (pt::ptree::value_type &groupsXml : databaseXml.get_child("groups"))
            {
                pt::ptree &groupXml = groupsXml.second;

                string groupId           = groupXml.get<string>("<xmlattr>.id", "");
                string groupRefs         = groupXml.get<string>("<xmlattr>.refs", "");
                string groupDescription  = groupXml.get<string>("description", "");
                
				GroupSourceType groupSourceType   = GroupSourceUtil::parse(groupXml.get<string>("<xmlattr>.sourceType", ""));
				string			groupSourceMethod = groupXml.get<string>("<xmlattr>.sourceMethod", "");

                for (pt::ptree::value_type &processesXml : groupXml.get_child("processes"))
                {
                    if (processesXml.first == "process")
                    {
                        pt::ptree &processXml = processesXml.second;

                        int processId       = processXml.get<int>("<xmlattr>.id", 0);
                        string processRefs  = processXml.get<string>("<xmlattr>.refs", "");

                        try
                        {
                            TableType              		  processTableType              = TableTypeUtil::parse(processXml.get<string>("<xmlattr>.tableType", ""));
                            ProcessCollisionType          processCollisionType          = ProcessCollisionUtil::parse(processXml.get<string>("<xmlattr>.collisionType", ""));
                            ProcessCollisionInelasticType processCollisionInelasticType = ProcessCollisionInelasticUtil::parse(processXml.get<string>("<xmlattr>.inelasticType", ""));
                            ProcessDirectionType          processDirectionType          = ProcessDirectionUtil::parse(processXml.get<string>("<xmlattr>.directionType", ""));
                            int          				  processMomentOrder            = ProcessMomentOrderUtil::parse(processXml.get<string>("<xmlattr>.momentOrder", ""));

                            bool processDissociativeFlag = processXml.get<bool>("<xmlattr>.dissociative", false);

                            vector<Specie>                            reactants;
                            vector<vector<Specie>>                    productsSpecies;
                            vector<std::optional<QtySiDimensionless>> productsWeights;

                            if (processXml.count("reactants"))
                            {
                                for (pt::ptree::value_type &reactantsXml : processXml.get_child("reactants"))
                                {
                                    if (reactantsXml.first == "electron")
                                    {
                                        reactants.push_back(Specie(ElectronSpecie()));
                                    } else if (reactantsXml.first == "molecule")
                                    {
                                        string formula    = reactantsXml.second.get_value<string>();
                                        int    ionization = reactantsXml.second.get<int>("<xmlattr>.charge", 0);
                                        string state      = reactantsXml.second.get<string>("<xmlattr>.state", "");
                                        string isomer      = reactantsXml.second.get<string>("<xmlattr>.isomer", "");

                                        Molecule molecule(formula);
                                        if (!isomer.empty())
                                            molecule.setIsomer(isomer);

                                        reactants.push_back(Specie(IonSpecie(molecule, ionization, state)));
                                    }
                                }
                            }

                            for (pt::ptree::value_type &productsXml : processXml)
                            {
                                if (productsXml.first == "products")
                                {
                                    vector<Specie> products;
                                    for (pt::ptree::value_type &productsXml : productsXml.second)
                                    {
                                        if (productsXml.first == "electron")
                                        {
                                            products.push_back(Specie(ElectronSpecie()));
                                        }
                                        else if (productsXml.first == "molecule")
                                        {
                                            string formula    = productsXml.second.get_value<string>();
                                            int    ionization = productsXml.second.get<int>("<xmlattr>.charge", 0);
                                            string state      = productsXml.second.get<string>("<xmlattr>.state", "");
                                            string isomer     = productsXml.second.get<string>("<xmlattr>.isomer", "");

                                            Molecule molecule(formula);
                                            if (!isomer.empty())
                                                molecule.setIsomer(isomer);

                                            products.push_back(Specie(IonSpecie(molecule, ionization, state)));
                                        }
                                    }

                                    productsSpecies.push_back(products);

                                    if (productsXml.second.count("<xmlattr>.weight"))
                                        productsWeights.push_back(productsXml.second.get<double>("<xmlattr>.weight"));
                                    else
                                        productsWeights.push_back(std::optional<QtySiDimensionless>());


                                }
                            }



                            map<string,std::any> parameters;
                            for (pt::ptree::value_type &parametersXml : processXml.get_child("parameters"))
                            {
                                if (parametersXml.first == "parameter")
                                {
                                    string name    = parametersXml.second.get<string>("<xmlattr>.name", "");;
                                    string units   = parametersXml.second.get<string>("<xmlattr>.units", "");;
                                    string value   = parametersXml.second.get_value<string>();

                                    if (units.empty())
                                    {
                                        if (value == "true")
                                            parameters[name] = true;
                                        else if (value == "false")
                                            parameters[name] = false;
                                        else
                                        {
                                            try
                                            {
                                                parameters[name] = stoi(value);
                                            }
                                            catch(const invalid_argument&)
                                            {
                                                try
                                                {
                                                    parameters[name] = QtySiDimensionless(stod(value));
                                                }
                                                catch(const invalid_argument&)
                                                {
                                                    parameters[name] = name;
                                                }
                                            }
                                        }
                                    }
                                    else if (units == "eV")
                                        parameters[name] = stod(value) * ZCrossTypes::electronvoltEnergy;
                                    else
                                        throw new ZCrossError("Unable to decode the '" + units + "' unit of the parameter '" + name + "'")

                                }
                            }

                            string processComments = processXml.get<string>("comment", "");

                            tm processLastUpdateTm;
                            strptime(processXml.get<string>("updated", "").c_str(), "%Y-%m-%dT%H:%M:%S", &processLastUpdateTm);
                            chrono::time_point<chrono::system_clock> processLastUpdate = chrono::system_clock::from_time_t(mktime(&processLastUpdateTm));

                            unsigned int size = processXml.get<unsigned int>("size", 0);


                            vector<QtySiEnergy>          energies;
                            vector<QtySiArea>            areas;
                            vector<QtySiPlaneAngle>     angles;
                            vector<QtySiAreaPerSolidAngle> areasSolid;

                            if (processTableType == TableType::INTEGRAL)
                            {
                                if (processXml.get<string>("data_x.<xmlattr>.type") != "energy")
                                    throw ZCrossError("data_x must contains energy values");
                                if (processXml.get<string>("data_y.<xmlattr>.type") != "cross_section")
                                    throw ZCrossError("data_y must contains energy values");

                                string unitX = processXml.get<string>("data_x.<xmlattr>.units");
                                string unitY = processXml.get<string>("data_y.<xmlattr>.units");

                                stringstream streamEnergies(processXml.get<string>("data_x", ""));
                                stringstream streamAreas(processXml.get<string>("data_y", ""));


                                for (unsigned int i = 0; i < size; i++)
                                {
                                    double value;
                                    streamEnergies >> value;
                                    energies.push_back(parseEnergy(value, unitX));
                                }

                                for (unsigned int i = 0; i < size; i++)
                                {
                                    double value;
                                    streamAreas >> value;
                                    areas.push_back(parseArea(value, unitY));
                                }
                            } else if (processTableType == TableType::DIFFERENTIAL)
                            {
                                if (processXml.get<string>("data_x.<xmlattr>.type") != "energy")
                                    throw ZCrossError("data_x must contains energy values");
                                if (processXml.get<string>("data_y.<xmlattr>.type") != "angle")
                                    throw ZCrossError("data_y must contains angle values");
                                if (processXml.get<string>("data_z.<xmlattr>.type") != "cross_section")
                                    throw ZCrossError("data_z must contains cross section values");

                                string unitX = processXml.get<string>("data_x.<xmlattr>.units");
                                string unitY = processXml.get<string>("data_y.<xmlattr>.units");
                                string unitZ = processXml.get<string>("data_z.<xmlattr>.units");

                                stringstream streamEnergies(processXml.get<string>("data_x", ""));
                                stringstream streamAngles(processXml.get<string>("data_y", ""));
                                stringstream streamAreasSolid(processXml.get<string>("data_z", ""));

                                for (unsigned int i = 0; i < size; i++)
                                {
                                    double value;
                                    streamEnergies >> value;
                                    energies.push_back(parseEnergy(value, unitX));
                                }

                                for (unsigned int i = 0; i < size; i++)
                                {
                                    double value;
                                    streamAngles >> value;
                                    angles.push_back(parsePlaneAngle(value, unitY));
                                }

                                for (unsigned int i = 0; i < size; i++)
                                {
                                    double value;
                                    streamAreasSolid >> value;
                                    areasSolid.push_back(parseAreaPerSolidAngle(value, unitZ));
                                }
                            }


                            if (processTableType == TableType::INTEGRAL)
                            {

                                IntScatteringTable cs;

                                cs.setDatabaseId(databaseId);
                                cs.setDatabaseName(databaseName);
                                cs.setDatabaseUrl(databaseUrl);
                                cs.setDatabaseDescription(databaseDescription);
                                cs.setDatabaseContacts(databaseContacts);
                                cs.setDatabaseReferenceIds(splitByComma(databaseRefs));

                                cs.setGroupId(groupId);
                                cs.setGroupDescription(groupDescription);
                                cs.setGroupReferenceIds(splitByComma(groupRefs));
                                cs.setGroupSourceType(groupSourceType);
                                cs.setGroupSourceMethod(groupSourceMethod);

                                cs.setProcessId(processId);
                                cs.setProcessReferenceIds(splitByComma(processRefs));

                                Reaction rc;

                                rc.setBullet(reactants.at(0));
                                rc.setTarget(reactants.at(1));

                                int channel = 0;
                                for (const auto& products: productsSpecies)
                                {
                                    for (const Specie &product: products)
                                        rc.addProduct(channel, product, productsWeights.at(channel));

                                    channel++;
                                }

                                // Completing reaction (for inelastic reactions)
                                if (processCollisionType == ProcessCollisionType::INELASTIC)
                                    rc.completeProductFragments();

                                    
                                // Completing reaction (for elastic reactions)
                                if (processCollisionType == ProcessCollisionType::ELASTIC && !processDissociativeFlag && rc.getProducts().empty())
                                    rc.generateProductsElastic();
                                  
                                // Completing reaction (for inelastic reactions)  
                                if (processCollisionType == ProcessCollisionType::INELASTIC && !processDissociativeFlag && rc.getProducts().empty())
                                {
                                    
                                    switch (processCollisionInelasticType)
                                    {
										case ProcessCollisionInelasticType::IONIZATION:
											rc.generateProductsIonization();
										break;
										
										case ProcessCollisionInelasticType::ATTACHMENT:
											rc.generateProductsAttachment();
										break;
										
										default:
										break;
									}
                                }
                                
                                Process p;
                                p.setCollisionType(processCollisionType);
                                p.setCollisionInelasticType(processCollisionInelasticType);
                                p.setDirectionType(processDirectionType);
                                p.setMomentOrder(processMomentOrder);
                                p.setDissociativeFlag(processDissociativeFlag);
                                p.setReaction(rc);
                                p.setComments(processComments);
                                p.setParameters(parameters);
                                cs.setProcess(p);

                                cs.setProcessLastUpdate(processLastUpdate);

                                map<QtySiEnergy, QtySiArea> table;
                                for (unsigned int i = 0; i < energies.size(); i++)
                                {
                                    if(areas[i]    < QtySiArea())   throw ZCrossError("Cross-section area values should be non-negative");
                                    if(energies[i] < QtySiEnergy()) throw ZCrossError("Cross-section enrgy values should be non-negative");

                                    table[energies[i]] = areas[i];
                                }

                                cs.setTable(table);

                                //cs.getTableContent();
                                // Checking if everything is ok
                                cs.validate();

                                const Specie &bullet = cs.getProcess().getReaction().getReactants().getBullet();
                                const Specie &target = cs.getProcess().getReaction().getReactants().getTarget();


                                if (intScatteringTables.count(bullet) == 0)
                                    intScatteringTables[bullet] = unordered_map<const Specie, vector<IntScatteringTable>>();

                                if (target.isIon())
                                {
                                    if (intScatteringTables[bullet].count(target) == 0)
                                        intScatteringTables[bullet][target] = vector<IntScatteringTable>();

                                    intScatteringTables[bullet][target].push_back(cs);
                                }

                            } else if (processTableType == TableType::DIFFERENTIAL)
                            {
                                DifScatteringTable cs;

                                cs.setDatabaseId(databaseId);
                                cs.setDatabaseName(databaseName);
                                cs.setDatabaseUrl(databaseUrl);
                                cs.setDatabaseDescription(databaseDescription);
                                cs.setDatabaseContacts(databaseContacts);
                                cs.setDatabaseReferenceIds(splitByComma(databaseRefs));

                                cs.setGroupId(groupId);
                                cs.setGroupDescription(groupDescription);
                                cs.setGroupReferenceIds(splitByComma(groupRefs));
                                cs.setGroupSourceType(groupSourceType);
                                cs.setGroupSourceMethod(groupSourceMethod);

                                cs.setProcessId(processId);
                                cs.setProcessReferenceIds(splitByComma(processRefs));

                                Reaction rc;

                                rc.setBullet(reactants.at(0));
                                rc.setTarget(reactants.at(1));

                                int channel = 0;
                                for (const auto& products: productsSpecies)
                                {
                                    for (const Specie &product: products)
                                        rc.addProduct(channel, product, productsWeights.at(channel));
                                    channel++;
                                }

                                // Completing reaction (for inelastic reactions)
                                if (processCollisionType == ProcessCollisionType::INELASTIC)
                                    rc.completeProductFragments();
                                
                                
                                // Completing reaction (for elastic reactions)
                                if (processCollisionType == ProcessCollisionType::ELASTIC && !processDissociativeFlag && rc.getProducts().empty())
                                    rc.generateProductsElastic();
                                
                                // Completing reaction (for inelastic reactions)  
                                if (processCollisionType == ProcessCollisionType::INELASTIC && !processDissociativeFlag && rc.getProducts().empty())
                                {
                                    
                                    switch (processCollisionInelasticType)
                                    {
										case ProcessCollisionInelasticType::IONIZATION:
											rc.generateProductsIonization();
										break;
										
										case ProcessCollisionInelasticType::ATTACHMENT:
											rc.generateProductsAttachment();
										break;
										
										default:
										break;
									}
                                }

                                Process p;
                                p.setCollisionType(processCollisionType);
                                p.setCollisionInelasticType(processCollisionInelasticType);
                                p.setDirectionType(processDirectionType);
                                p.setMomentOrder(processMomentOrder);
                                p.setDissociativeFlag(processDissociativeFlag);
                                p.setReaction(rc);
                                p.setComments(processComments);
                                p.setParameters(parameters);
                                cs.setProcess(p);

                                cs.setProcessLastUpdate(processLastUpdate);

                                for (unsigned int i = 0; i < energies.size(); i++)
                                {
                                    if (areas[i]    < QtySiArea())       throw ZCrossError("Cross-section area values should be non-negative");
                                    if (angles[i]   < QtySiPlaneAngle()) throw ZCrossError("Cross-section angle values should be non-negative");
                                    if (energies[i] < QtySiEnergy())     throw ZCrossError("Cross-section energy values should be non-negative");
                                    cs.addEntry(energies[i], angles[i], areasSolid[i]);
                                }


                                // Checking if everything is ok
                                cs.validate();

                                const Specie &bullet = cs.getProcess().getReaction().getReactants().getBullet();
                                const Specie &target = cs.getProcess().getReaction().getReactants().getTarget();


                                if (difScatteringTables.count(bullet) == 0)
                                    difScatteringTables[bullet] = unordered_map<const Specie, vector<DifScatteringTable>>();

                                if (target.isIon())
                                {
                                    if (difScatteringTables[bullet].count(target) == 0)
                                        difScatteringTables[bullet][target] = vector<DifScatteringTable>();

                                    difScatteringTables[bullet][target].push_back(cs);
                                }
                            }
                        }
                        catch (const ZCrossError &e) // todo: revert back to std::exception
                        {
                            throw ZCrossErrorFwd("Error while parsing process '" + databaseId + "/" + groupId + "/" + to_string(processId) + "'", e)
                        }
                    }
                }
            }
        }
    }
}




const std::vector<IntScatteringTable> ZCross::getIntScatteringTables(const Specie &bulletSpecie,
                                                                     const Specie &targetSpecie) const
{
    try
    {
        return intScatteringTables.at(bulletSpecie).at(targetSpecie);
    }
    catch (const out_of_range &e)
    {
        throw ZCrossError("Unable to find a scattering table with BULLET specie '" + bulletSpecie.toString(PrintMode::BASIC) + "' and TARGET specie '" + targetSpecie.toString(PrintMode::BASIC) + "'");
    }
}

const std::vector<IntScatteringTable> ZCross::getIntScatteringTables(const Specie &bulletSpecie) const
{

    vector<IntScatteringTable> buffer;

    try
    {
        for (auto entryTarget: intScatteringTables.at(bulletSpecie))
        {
            buffer.insert(buffer.end(), entryTarget.second.begin(), entryTarget.second.end());
        }
    }
    catch (const std::out_of_range &e)
    {
        throw ZCrossError("Unable to find a scattering table with BULLET specie '" + bulletSpecie.toString(PrintMode::BASIC) + "'")
    }

    return buffer;
}

const std::vector<IntScatteringTable> ZCross::getIntScatteringTables() const
{
    vector<IntScatteringTable> buffer;

    for (auto entryBullet: intScatteringTables)
    {
        for (auto entryTarget: intScatteringTables.at(entryBullet.first))
        {
            buffer.insert(buffer.end(), entryTarget.second.begin(), entryTarget.second.end());
        }
    }

    return buffer;
}

const std::vector<IntScatteringTable> ZCross::getIntScatteringTables(const BaseProcessFilter &filter) const
{
    vector<IntScatteringTable> buffer;

    for (const auto& entryBullet: intScatteringTables)
    {
        for (const auto& entryTarget: intScatteringTables.at(entryBullet.first))
        {
            for (const auto& table: entryTarget.second)
            {
                if (filter.accept(table))
                    buffer.push_back(table);
            }
        }
    }

    return buffer;
}

const std::vector<DifScatteringTable> ZCross::getDifScatteringTables(const Specie &bulletSpecie, const Specie &targetSpecie) const
{
    try
    {
        return difScatteringTables.at(bulletSpecie).at(targetSpecie);;
    }
    catch (const out_of_range &e)
    {
        throw ZCrossError("Unable to find a differential scattering table with BULLET specie '" + bulletSpecie.toString(PrintMode::BASIC) + "' and TARGET specie '" + targetSpecie.toString(PrintMode::BASIC) + "'");
    }

}

const std::vector<DifScatteringTable> ZCross::getDifScatteringTables(const Specie &bulletSpecie) const
{

    vector<DifScatteringTable> buffer;

    try
    {
        for (auto entryTarget: difScatteringTables.at(bulletSpecie))
        {
            buffer.insert(buffer.end(), entryTarget.second.begin(), entryTarget.second.end());
        }
    }
    catch (const std::out_of_range &e)
    {
        throw ZCrossError("Unable to find a differential scattering table with BULLET specie '" + bulletSpecie.toString(PrintMode::BASIC) + "'")
    }

    return buffer;
}

const std::vector<DifScatteringTable> ZCross::getDifScatteringTables() const
{
    vector<DifScatteringTable> buffer;

    for (auto entryBullet: difScatteringTables)
    {
        for (auto entryTarget: difScatteringTables.at(entryBullet.first))
        {
            buffer.insert(buffer.end(), entryTarget.second.begin(), entryTarget.second.end());
        }
    }

    return buffer;
}

const std::vector<DifScatteringTable> ZCross::getDifScatteringTables(const BaseProcessFilter &filter) const
{
    vector<DifScatteringTable> buffer;

    for (auto entryBullet: difScatteringTables)
    {
        for (auto entryTarget: difScatteringTables.at(entryBullet.first))
        {
            for (auto table: entryTarget.second)
            {
                if (filter.accept(table))
                    buffer.push_back(table);
            }
        }
    }

    return buffer;
}

std::vector<Reference> ZCross::getDatabaseReferences(const BaseTable &table) const
{
    vector<Reference> result;

    const auto& it = references.find(table.getDatabaseId());
    
    if (it != references.end())
    {
		const auto& refs = it->second;

		for (int id: table.getDatabaseReferenceIds())
			result.push_back(refs.at(id));
	}
	
    return result;
}

std::vector<Reference> ZCross::getGroupReferences(const BaseTable &table) const
{
    vector<Reference> result;

    const auto& it = references.find(table.getDatabaseId());
    
    if (it != references.end())
    {
		const auto& refs = it->second;

		for (int id: table.getGroupReferenceIds())
			result.push_back(refs.at(id));
	}
	
    return result;
}

std::vector<Reference> ZCross::getProcessReferences(const BaseTable &table) const
{
    vector<Reference> result;

	const auto& it = references.find(table.getDatabaseId());
	
	if (it != references.end())
	{
		const auto& refs = it->second;

		for (int id: table.getProcessReferenceIds())
			result.push_back(refs.at(id));
	}
	
    return result;
}


const std::set<std::string> ZCross::getDatabasesIds() const
{
    set<std::string> result;
    for (auto entryBullet: intScatteringTables)
    {
        for (auto entryTarget: intScatteringTables.at(entryBullet.first))
        {
            for (const IntScatteringTable& table: entryTarget.second)
                result.insert(table.getDatabaseId());
        }
    }
    return result;
}

const std::map<std::string, std::set<std::string>> ZCross::getGroupsIds() const
{
    std::map<std::string, std::set<std::string>> result;
    for (auto entryBullet: intScatteringTables)
    {
        for (auto entryTarget: intScatteringTables.at(entryBullet.first))
        {
            for (const IntScatteringTable& table: entryTarget.second)
                result[table.getDatabaseId()].insert(table.getGroupId());
        }
    }
    return result;
}

const std::map<std::string, std::map<std::string, std::set<int>>> ZCross::getProcessesIds() const
{
    std::map<std::string, std::map<std::string, std::set<int>>> result;
    for (auto entryBullet: intScatteringTables)
    {
        for (auto entryTarget: intScatteringTables.at(entryBullet.first))
        {
            for (const IntScatteringTable& table: entryTarget.second)
                result[table.getDatabaseId()][table.getGroupId()].insert(table.getProcessId());
        }
    }
    return result;
}

const std::set<TableId> ZCross::getTableIds() const
{
    std::set<TableId> result;
    for (auto entryBullet: intScatteringTables)
    {
        for (auto entryTarget: intScatteringTables.at(entryBullet.first))
        {
            for (const IntScatteringTable& table: entryTarget.second)
                result.insert(table.getId());
        }
    }
    return result;
}
