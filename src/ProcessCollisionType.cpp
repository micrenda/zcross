
#include <string>
#include "zcross/ProcessCollisionType.hpp"
#include "zcross/ZCrossError.hpp"

using namespace std;

namespace dfpe
{
    const std::string ProcessCollisionUtil::getName(const ProcessCollisionType& type)
    {
        switch(type)
        {
            case ProcessCollisionType::TOTAL:
                return "total";
            case ProcessCollisionType::ELASTIC:
                return "elastic";
            case ProcessCollisionType::INELASTIC:
                return "inelastic";
            case ProcessCollisionType::SUPERELASTIC:
                return "superelastic";
        }

        throw ZCrossError("Unknown process collision type. Contact developers.");
    }
    
    const std::string ProcessCollisionUtil::getDesc(const ProcessCollisionType& type)
    {
        switch(type)
        {
            case ProcessCollisionType::TOTAL:
                return "Total";
            case ProcessCollisionType::ELASTIC:
                return "Elastic";
            case ProcessCollisionType::INELASTIC:
                return "Inelastic";
            case ProcessCollisionType::SUPERELASTIC:
                return "Superelastic";
        }

        throw ZCrossError("Unknown process collision type. Contact developers.");
    }

    ProcessCollisionType ProcessCollisionUtil::parse(const string& s)
    {
        if (s == "total")
            return ProcessCollisionType::TOTAL;
        else if (s == "elastic")
            return ProcessCollisionType::ELASTIC;
        else if (s == "inelastic")
            return ProcessCollisionType::INELASTIC;
        else if (s == "superelastic")
            return ProcessCollisionType::SUPERELASTIC;
        else
            throw ZCrossError("Unknown process collision type: '" + s + "'. Contact developers.");
    }
}


