#include <boost/units/quantity.hpp>
#include <boost/units/quantity.hpp>
#include <string>
#include <string>
#include <system_error>
#include <system_error>

#include "zcross/Specie.hpp"
#include "zcross/Element.hpp"
#include "zcross/Hashes.hpp"
#include "zcross/IonSpecie.hpp"
#include "zcross/Molecule.hpp"
#include "zcross/MoleculeComponent.hpp"
#include "zcross/Reference.hpp"

using namespace dfpe;

std::size_t std::hash<const Specie>::operator()(const Specie &k) const
{
    return hash<Specie>()(k);
}

std::size_t std::hash<Specie>::operator()(const Specie &k) const
{
    using std::size_t;
    using std::hash;
    using std::string;

    if (k.isParticle())
        return hash<ParticleSpecie>()(k.getParticle());
    if (k.isIon())
        return hash<IonSpecie>()(k.getIon());

    return 0;
}

std::size_t std::hash<Element>::operator()(const Element &k) const {
    using std::size_t;
    using std::hash;
    using std::string;

    return    hash<unsigned int>()(k.getZ());
}

std::size_t std::hash<ParticleSpecie>::operator()(const ParticleSpecie &) const {
    using std::size_t;
    using std::hash;
    using std::string;

    return 0;
}

std::size_t std::hash<IonSpecie>::operator()(const IonSpecie &k) const {
    using std::size_t;
    using std::hash;
    using std::string;

    return  hash<Molecule>()(k.getMolecule())^
            hash<int>()(k.getIonization());
            hash<std::string>()(k.getState());
}

std::size_t std::hash<Molecule>::operator()(const Molecule &k) const {
    using std::size_t;
    using std::hash;
    using std::string;

    std::size_t h = 0;
    for (unsigned int i = 0; i < k.getSize() ; ++i)
    {
        h ^= hash<MoleculeComponent>()(k.getComponent(i));
        h ^= hash<unsigned int>()(k.getQuantity(i));
    }

    h ^= hash<std::optional<std::string>>()(k.getIsomer());

    return h;
}

std::size_t std::hash<MoleculeComponent>::operator()(const MoleculeComponent &k) const {
    using std::size_t;
    using std::hash;
    using std::string;

    std::size_t h = 0;

    if(k.isElement())
        h ^= hash<Element>()(k.getElement());
    else if(k.isMolecule())
        h ^= hash<Molecule>()(k.getMolecule());

    return h;
}

std::size_t std::hash<Reference>::operator()(const Reference &k) const {
    using std::size_t;
    using std::hash;
    using std::string;

    std::size_t h = 0;

    h ^= hash<string>()(k.getType());
    h ^= hash<string>()(k.getLabel());
    
    for (const string& field: k.getFields())
        h ^= hash<string>()(field);
    for (const string& value: k.getValues())
        h ^= hash<string>()(value);

    return h;
}

