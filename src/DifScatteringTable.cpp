#include <boost/units/detail/one.hpp>
#include <boost/units/io.hpp>
#include <boost/units/systems/si.hpp>
#include <boost/units/systems/si/prefixes.hpp>
#include <zcross/PrintMode.hpp>
#include <boost/units/systems/angle/degrees.hpp>

#include "zcross/ZCrossError.hpp"
#include "zcross/DifScatteringTable.hpp"

using namespace std;
using namespace boost::units;
using namespace dfpe;

QtySiAreaPerSolidAngle DifScatteringTable::getInterpoledValue(
	const QtySiEnergy&      ,
	const QtySiPlaneAngle& ,
	const OutOfTableMode& ,
	const OutOfTableMode& ) const
{
    //std::map<QtySiEnergy, QtySiArea>::iterator it = table.upper_bound(energy);

    //if (!table.empty())
    //{
        //if (it != table.end())
        //{
            //QtySiEnergy energyUpper = it->first;
            //QtySiArea   areaUpper   = it->second;

            //if (it != table.begin())
            //{
                //--it;

                //QtySiEnergy energyLower = it->first;
                //QtySiArea areaLower = it->second;

                //return areaLower + (areaUpper - areaLower) * (energy - energyLower) / (energyUpper - energyLower);

            //}
            //else
            //{
				//if (lowerBoundaryMode == OutOfTableMode::ZERO_VALUE)
					//return QtySiArea();
				//else if (lowerBoundaryMode == OutOfTableMode::BOUNDARY_VALUE)
					//return table.begin()->second;
				//else
				//{
					//stringstream ss;
					//ss << "Impossible to get a valid cross section '" << getShortDescription() << "' for energy " << (energy / ZCrossTypes::electronvoltEnergy).value() << " eV: the value is too low. Current table cover energies between " << (table.begin()->first / ZCrossTypes::electronvoltEnergy).value() << " eV and " << (table.rbegin()->first / ZCrossTypes::electronvoltEnergy).value() << "eV.";
					//throw ZCrossError(ss.str());
				//}
            //}
        //}
        //else
        //{
			//if (upperBoundaryMode == OutOfTableMode::ZERO_VALUE)
					//return QtySiArea();
			//else if (upperBoundaryMode == OutOfTableMode::BOUNDARY_VALUE)
				//return table.rbegin()->second;
			//else
			//{
				//stringstream ss;
				//ss << "Impossible to get a valid cross section '" << getShortDescription() << "' for energy " << (energy / ZCrossTypes::electronvoltEnergy).value() << " eV: the value is too high. Current table cover energies between " << (table.begin()->first /ZCrossTypes::electronvoltEnergy).value() << " eV and " << (table.rbegin()->first / ZCrossTypes::electronvoltEnergy).value() << " eV.";
				//throw ZCrossError(ss.str());
			//}
        //}
    //}
    //else
    //{
        //stringstream ss;
        //ss << "Impossible to get a valid cross section '" << getShortDescription() << "' for energy " << (energy / ZCrossTypes::electronvoltEnergy).value() << " eV: the current table is empty.";
        //throw ZCrossError(ss.str());
    //}

    throw ZCrossError("This function is still unimplemented");
}

void DifScatteringTable::addEntry(
	const QtySiEnergy&                                   energy, 
	const QtySiPlaneAngle&  angle,  
	const QtySiAreaPerSolidAngle&            section)
{
	
	if (table.count(energy) == 0)
		table[energy] = map<QtySiPlaneAngle,QtySiAreaPerSolidAngle>();
	
	table[energy][angle] = section;
}

unsigned int DifScatteringTable::getSize() const
{
	return table.size();
}

QtySiEnergy DifScatteringTable::getEnergyLowerBound() const
{
	return table.begin()->first;
}

QtySiEnergy DifScatteringTable::getEnergyUpperBound() const
{
	return table.rbegin()->first;
}

string DifScatteringTable::getTableContent(const PrintMode& mode) const
{
	stringstream ss;

	for (auto item: table)
	{
		ss << setw(4) << (item.first / ZCrossTypes::electronvoltEnergy).value() << " eV :" << endl;

		for (auto subitem: item.second)
		{
			ss << setw(10) << (subitem.first / QtySiPlaneAngle(1. * degree::degree)).value();
            if (mode == PrintMode::PRETTY)
              ss << "°";
            else
              ss << " deg";

            ss << " : " <<  setw(10) << (subitem.second / QtySiAreaPerSolidAngle(1. * si::centi * si::meter * si::centi * si::meter / si::steradian)).value();

			if (mode == PrintMode::PRETTY)
				ss << " cm²/Sr";
			else
				ss << " cm^2/Sr";

            ss << endl;
		}

	}

	return ss.str();
}

TableType DifScatteringTable::getProcessTableType() const {
    return TableType::DIFFERENTIAL;
}

QtySiEnergy DifScatteringTable::getEnergyThreshold() const
{
   throw ZCrossError("This function is still unimplemented");
}
