
#include "zcross/ProcessFilter.hpp"
#include <algorithm>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include "numeric"

using namespace dfpe;
using namespace std;

ProcessFilter::ProcessFilter(std::string s, bool exact) : exact(exact)
{
    vector<string> tokens;
    boost::algorithm::split( tokens, s, boost::is_any_of("/"));

    if (tokens.size() > 0 && tokens[0] != "*")
        addFilterByDatabaseId(tokens[0]);

    if (tokens.size() > 1 && tokens[1] != "*")
        addFilterByGroupId(tokens[1]);

    if (tokens.size() > 2 && tokens[2] != "*")
    {
        vector<string> processIds;

        boost::split(processIds,  tokens[2],  boost::is_any_of(","), boost::token_compress_on);

        for (string processId: processIds)
        {
            size_t comma = processId.find("-");
            if (comma != string::npos)
            {
                int from = stoi(processId.substr (0,comma));
                int to   = stoi(processId.substr (comma + 1));

                vector<int> ids(to-from+1);
                iota(ids.begin(), ids.end(), from);

                addFilterByProcessIds(ids);
            }
            else
                addFilterByProcessId(stoi(processId));
        }
    }
}


bool ProcessFilter::accept(const BaseTable& table) const
{
	const Process& process = table.getProcess();

	if (!bullets.empty())
	{
		const Specie bullet = process.getReaction().getReactants().getBullet();
		if (!exact)
		{
			SimilarPredicate bulletPredicate(bullet);
			if (find_if(bullets.begin(), bullets.end(), bulletPredicate) == bullets.end())
				return false;
		}
		else
		{
			if (find(bullets.begin(), 	 bullets.end(), bullet) == bullets.end())
				return false;
		}
		
	}
	
	if (!targets.empty())
	{
		const Specie target = process.getReaction().getReactants().getTarget();
		if (!exact)
		{
			SimilarPredicate targetPredicate(target);
			if (find_if(targets.begin(), targets.end(), targetPredicate) == targets.end())
				return false;
		}
		else
		{
			if (find(targets.begin(), 	 targets.end(), target) == targets.end())
				return false;
		}
	}
	
	//if (!databaseIds.empty() 	&& find(databaseIds.begin(), 	databaseIds.end(),  table.getDatabaseId()) == databaseIds.end())
	//	return false;
			
	if (!databaseIds.empty())
	{
		if (find_if(databaseIds.begin(), 	databaseIds.end(),  [&](auto const &value)->bool { return boost::iequals(table.getDatabaseId(), value); }) == databaseIds.end())
				return false;
	}
	

	
	if (!groupIds.empty()    	&& find(groupIds.begin(), 		groupIds.end(),  	table.getGroupId()) == groupIds.end())
		return false;
	if (!processIds.empty()    	&& find(processIds.begin(), 	processIds.end(),  	table.getProcessId()) == processIds.end())
		return false;
		
	if (!processCollisionTypes.empty()	&& find(processCollisionTypes.begin(), 	processCollisionTypes.end(), process.getCollisionType()) == processCollisionTypes.end())
		return false;

	if (!processCollisionInelasticTypes.empty()	&& 
               find(processCollisionInelasticTypes.begin(), 	
                    processCollisionInelasticTypes.end(),
                    process.getCollisionInelasticType()) == processCollisionInelasticTypes.end())
		return false;

	if (!processDirectionTypes.empty()	&& find(processDirectionTypes.begin(), 	processDirectionTypes.end(), process.getDirectionType()) == processDirectionTypes.end())
		return false;

	if (!processMomentOrders.empty()	&& find(processMomentOrders.begin(), 	processMomentOrders.end(), process.getMomentOrder()) == processMomentOrders.end())
		return false;

	return true;
}



bool ProcessFilterAny::accept(const BaseTable& table) const
{
	if (filters.empty())
		return true;
		
	for (const BaseProcessFilter& filter : filters)
		if (filter.accept(table))
			return true;
			
	return false;
}

bool ProcessFilterAll::accept(const BaseTable& table) const
{
	if (filters.empty())
		return true;
		
	for (const BaseProcessFilter& filter : filters)
		if (!filter.accept(table))
			return false;
			
	return true;
}

bool TableIdFilter::accept(const BaseTable& table) const
{
	if (tableIds.empty())
		return false;

	for (const TableId& tableId : tableIds)
        if (table.getId() == tableId)
            return true;

	return false;
}
