#include <zcross/ZCrossError.hpp>
#include "zcross/PDGTable.hpp"
#include "ZCrossConfig.hpp"
#include <set>
#include <fstream>
#include <boost/units/systems/si/codata/electromagnetic_constants.hpp>
#include <boost/units/systems/si/codata/universal_constants.hpp>

using namespace dfpe;
using namespace std;
using namespace std::filesystem;
using namespace boost::units;
using namespace boost::units::si::constants;

PDGTable::PDGTable() {

    path lookupPath = path(ZCROSS_INSTALL_PREFIX) / path("share/zcross/");

    set<path> files;

    if (exists(lookupPath))
    {
        for (const directory_entry & entry : directory_iterator(lookupPath))
        {
           if (entry.is_regular_file() && entry.path().extension().string() == ".mcd")
           {
                if (regex_match(entry.path().filename().string(),pdgFilenameRegex) )
                {
                    files.insert(entry.path());
                }
           }
        }

    }

    if (files.empty())
        throw ZCrossError("Unable to load the Particle Data Group dataset. Please download the dataset 'Tables of particle information' from 'https://pdg.lbl.gov' and save it under the directory '" + lookupPath.string() + "'" );

    load(*(files.rbegin()));
}

std::optional<ParticleSpecie> PDGTable::get(int id) const
{
    auto it = content.find(id);
    if (it != content.end())
        return it->second;
    else
        return std::optional<ParticleSpecie>();
}

std::optional<ParticleSpecie> PDGTable::get(const std::string& name) const
{
    // Checking if somehow in the name, it was passed an numeric id
    try
    {
        int id = stoi(name);
        if (id != 0)
            return get(id); // Passing to the other function!
    }
    catch (const invalid_argument &) {} // nothing to do. Continuing!

    auto it = find_if(content.begin(),content.end(), [&](const auto& v) { return v.second.getName() == name; } );
    if (it != content.end())
        return it->second;
    else
        return std::optional<ParticleSpecie>();
}

void PDGTable::load(const path &filename)
{
    ifstream infile(filename);

    string line;
    while (getline(infile, line))
    {
        if (line.empty() || line[0] == '*')
            continue; // skipping empty lines and comments

        vector<int> ids;
        std::string id1 = line.substr( 0, 8);
        std::string id2 = line.substr( 8, 8);
        std::string id3 = line.substr(16, 8);
        std::string id4 = line.substr(24, 8);

        id1.erase(remove_if(id1.begin(), id1.end(), ::isspace), id1.end());
        id2.erase(remove_if(id2.begin(), id2.end(), ::isspace), id2.end());
        id3.erase(remove_if(id3.begin(), id3.end(), ::isspace), id3.end());
        id4.erase(remove_if(id4.begin(), id4.end(), ::isspace), id4.end());

        if (!id1.empty()) ids.push_back(stoi(id1));
        if (!id2.empty()) ids.push_back(stoi(id2));
        if (!id3.empty()) ids.push_back(stoi(id3));
        if (!id4.empty()) ids.push_back(stoi(id4));

        std::string mass = line.substr(33, 17);
        mass.erase(remove_if(mass.begin(), mass.end(), ::isspace), mass.end());

        if (abs(stoi(id1)) < 10)
            continue; // Skip the quarks

        size_t nameStart = 107;
        size_t nameEnd = line.find(' ', 107);
        assert(nameEnd != string::npos);
        assert(nameStart != nameEnd);
        std::string name = line.substr(nameStart, nameEnd - nameStart);

        size_t chargesStart = line.rfind(' ');
        assert(chargesStart != string::npos);
        std::string last = line.substr(chargesStart);

        vector<int> charges;

        auto getCharge = [&](const string& s) { return count(s.begin(), s.end(), '+') - count(s.begin(), s.end(), '-');};

        size_t pos = 0;
        while ((pos = last.find(',')) != string::npos)
        {
            std::string token = last.substr(0, pos);
            token.erase(remove_if(token.begin(), token.end(), ::isspace), token.end());
            charges.push_back(getCharge(token));
            last.erase(0, pos + 1);
        }

        if (!last.empty())
            charges.push_back(getCharge(last));

        assert(ids.size() == charges.size());

        // Now we have all the data: filling the container

        for (size_t i = 0; i < ids.size(); i++)
        {
            QtySiMass restMass;
            if (!mass.empty())
                restMass = QtySiEnergy(stod(mass) * 1e9 * si::volt * codata::e) / (codata::c * codata::c);

            int id = ids[i];
            int charge = charges[i];

            content.emplace(
                    std::piecewise_construct,
                    std::forward_as_tuple(id),
                    std::forward_as_tuple(id, name, charge, restMass));

                string antiName;
                if (charges[i] != 0)
                    antiName = name + (charge < 0 ? "^+"  : "^-");
                else
                    antiName = "anti-" + name;

                content.emplace(
                        std::piecewise_construct,
                        std::forward_as_tuple(-id),
                        std::forward_as_tuple(-id, antiName, -charge, restMass));
        }
    }

    infile.close();
}

const PDGTable PDGTable::instance;
