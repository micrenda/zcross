
#include <string>
#include "zcross/GroupSourceType.hpp"
#include "zcross/ZCrossError.hpp"

using namespace std;

namespace dfpe
{
    const std::string GroupSourceUtil::getName(const GroupSourceType& type)
    {
        switch(type)
        {
            case GroupSourceType::EXPERIMENT:
                return "experiment";
            case GroupSourceType::THEORY:
                return "theory";
            case GroupSourceType::MIXED:
                return "mixed";
        }

        throw ZCrossError("Unknown group source type. Contact developers.");
    }
    
    const std::string GroupSourceUtil::getDesc(const GroupSourceType& type)
    {
        switch(type)
        {
            case GroupSourceType::EXPERIMENT:
                return "Experimental measurements";
            case GroupSourceType::THEORY:
                return "Theoretical calculations";
            case GroupSourceType::MIXED:
                return "Mixed sources";
        }

        throw ZCrossError("Unknown group source type. Contact developers.");
    }

    GroupSourceType GroupSourceUtil::parse(const string& s)
    {
        if (s == "experiment")
            return GroupSourceType::EXPERIMENT;
        else if (s == "theory")
            return GroupSourceType::THEORY;
        else if (s == "mixed")
            return GroupSourceType::MIXED;
        else
            throw ZCrossError("Unknown group source type: '" + s + "'. Contact developers.");
    }
}


