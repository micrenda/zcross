
#include "zcross/Reference.hpp"
#include "zcross/ZCrossError.hpp"
#include <boost/regex.hpp>
#include <zcross/PrintMode.hpp>
#include <iomanip>

using namespace std;
using namespace dfpe;

const boost::regex Reference::referenceRegex("^\\s*\\@(\\w+)\\s*\\{\\s*([\\w\\-_]+)\\s*\\,(.*)\\}\\s*$");
const boost::regex Reference::fieldRegex("\\s*(\\w+)\\s*");
const boost::regex Reference::equalRegex("\\s*\\=\\s*");
const boost::regex Reference::commaRegex("\\s*\\,?\\s*");

Reference Reference::parse(const string &s)
{
    Reference reference;

    boost::regex_constants::match_flag_type flags = boost::regex_constants::match_default  ;

    boost::smatch what;
    if (boost::regex_match(s, what, referenceRegex, flags))
    {
        reference.type  = what[1];
        reference.label = what[2];

        string s = what[3];


        string field;
        string value;

        string::const_iterator it = s.begin();

        boost::smatch what2;
        while (true)
        {
            if (it == s.end())
                break;

            if (!regex_search(it, s.cend(), what2, fieldRegex))
                throw ZCrossError("Unable to parse (a): " + s);

            it = what2[0].second;

            field = what2[1];

            if (!regex_search(it, s.cend(), what2, equalRegex))
                throw ZCrossError("Unable to parse (b): " + s);

            it = what2[0].second;


            // Paring balanced parentesis
            int level = 0;
            while (level > 0 || (level == 0 && *it != ' ' && *it != ',') )
            {
                if (*it == '{')
                    level++;
                if (*it == '}')
                    level--;

                char c = *it;
                value += c;

                it++;

                if (it == s.cend())
                    break;
            }


            if (value.size() >= 2 && value[0] == '{' && value[value.size() - 1] == '}')
                value = string(value.begin() + 1, value.end() - 1);


            reference.keys.push_back(field);
            reference.values.push_back(value);

            field = "";
            value = "";


            if (!regex_search(it, s.cend(), what2, commaRegex))
                throw ZCrossError("Unable to parse (c): " + s);
            it = what2[0].second;


            if (it == s.cend())
                break;
        }
    }
    else
        throw ZCrossError("Unable to parse bibtex reference: " + s);


    return reference;
}

const string& Reference::getValue(const string& field) const
{
    auto it = find(keys.begin(), keys.end(), field);
    if ( it != keys.end())
    {
        return values[it - keys.begin()];
    }

    throw ZCrossError("Unknown field '" + field + "' in reference '" + label + "'");
}

bool Reference::hasField(const string& field) const
{
    return find(keys.begin(), keys.end(), field) != keys.end();
}

const string Reference::toString(const PrintMode &printMode) const
{
    stringstream ss;

    if (printMode != PrintMode::LATEX)
    {
        ss << label << " (" << type << ")" << endl;

        auto itKey   = keys.begin();
        auto itValue = values.begin();

        while (true)
        {
            ss << setw(16) << *itKey << " = " << *itValue << endl;

            itKey++;
            itValue++;

            if (itKey == keys.end() || itValue == values.end())
                break;
        }
    }
    else
    {
        ss << "@" << type << "{" << label << ", " << endl;

        auto itKey   = keys.begin();
        auto itValue = values.begin();

        while (true)
        {
            ss << "  " << *itKey << " = {" << *itValue << "}, " << endl;

            itKey++;
            itValue++;

            if (itKey == keys.end() || itValue == values.end())
                break;
        }

        ss << "}" << endl;

    }
    return ss.str();
}

bool Reference::operator==(const Reference &rhs) const
{
    return type == rhs.type &&
           label == rhs.label &&
           keys == rhs.keys &&
           values == rhs.values;
}

bool Reference::operator!=(const Reference &rhs) const
{
    return !(rhs == *this);
}

