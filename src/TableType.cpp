#include <string>
#include <zcross/ZCrossError.hpp>
#include <zcross/TableType.hpp>

using namespace std;

namespace dfpe
{
    const std::string TableTypeUtil::getDesc(const TableType& type)
    {
        switch(type)
        {
            case TableType::INTEGRAL:
                return "Integral Scattering Cross Section";
            case TableType::DIFFERENTIAL:
                return "Differential Scattering Cross Section";
        }

        throw ZCrossError("Unknown process table type. Contact developers.");
    }
    
    const std::string TableTypeUtil::getName(const TableType& type)
    {
        switch(type)
        {
            case TableType::INTEGRAL:
                return "integral";
            case TableType::DIFFERENTIAL:
                return "differential";
        }

        throw ZCrossError("Unknown process table type. Contact developers.");
    }

    TableType TableTypeUtil::parse(const string& s)
    {
        if (s == "integral")
            return TableType::INTEGRAL;
        else if (s == "differential")
            return TableType::DIFFERENTIAL;
        else
            throw ZCrossError("Unknown process table type: '" + s + "'. Contact developers.");
    }

}
