#include "zcross/ZCross.hpp"
#include <algorithm>
#include <iostream>

using namespace dfpe;
using namespace std;

int main()
{
	ZCross parser;
	
	parser.loadDatabase("itikawa");

	ProcessFilter filter;
	filter.addFilterByDatabaseId("Itikawa");
	filter.addFilterByGroupId("CO2");
	
	vector<IntScatteringTable> intTables =  parser.getIntScatteringTables(filter);

	//sort(tables.begin(), tables.end());

	for (const IntScatteringTable& table: intTables)
		cout << left 
		<< setw (16) << table.getDatabaseId()  
		<< setw (16) << table.getGroupId()  
		<< setw (6) << table.getProcessId()
		<< setw (16) << table.getShortDescription()
		<< setw (60) << table.getProcess().getReaction().toString(PrintMode::PRETTY)
		<< setw (48) << table.getProcess().getComments()
		<< endl;


}
