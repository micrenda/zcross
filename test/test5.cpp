#include "zcross/ZCross.hpp"
#include "zcross/ProcessCollisionType.hpp"
#include <algorithm>
#include <iostream>
#include <boost/units/io.hpp>
#include <boost/units/quantity.hpp>
#include <boost/units/systems/si.hpp>
#include <boost/units/systems/si/prefixes.hpp>
#include <boost/units/systems/si/codata/electromagnetic_constants.hpp>

using namespace dfpe;
using namespace std;
using namespace boost::units;

int main()
{
	ZCross parser;

	parser.loadDatabase("biagi-7.1");
	parser.loadDatabase("biagi");
	parser.loadDatabase("bordage");
	parser.loadDatabase("bsr");
	parser.loadDatabase("ccc");
	parser.loadDatabase("christophorou");
	parser.loadDatabase("flinders");
	parser.loadDatabase("hayashi");
	parser.loadDatabase("itikawa");
	parser.loadDatabase("phelps");

	parser.loadDatabase("christophorou_differential");
	parser.loadDatabase("anu_differential");
	parser.loadDatabase("sophia_differential");

	ProcessFilter filter;
	filter.addFilterByProcessCollisionType(ProcessCollisionType::ELASTIC);

	vector<DifScatteringTable> intTables =  parser.getDifScatteringTables(filter);

	//sort(tables.begin(), tables.end());
    const DifScatteringTable& table = intTables[0];


	cout << left
		 << setw (16) << table.getDatabaseId()
		 << setw (16) << table.getGroupId()
		 << setw (6) << table.getProcessId()
		 << setw (48) << table.getProcess().getComments()
		 << endl;


	quantity<si::energy> energyEv(1. * si::volt * si::constants::codata::e);
	quantity<si::energy> energyStep(0.5 * energyEv);
	quantity<si::area> areaCm2(1. * si::centi * si::meter * si::centi * si::meter);

    cout << table.getTableContent(PrintMode::PRETTY) << endl;


}
