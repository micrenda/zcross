#include "zcross/ZCross.hpp"
#include <algorithm>
#include <iostream>

using namespace dfpe;
using namespace std;

int main()
{
	ZCross parser;


/*
    cout << "e   == e                     : "     << (ElectronSpecie() == ElectronSpecie() )                         << endl;
    cout << "CO2 == O2                    : "     << (IonSpecie(Molecule("CO2"))      == IonSpecie(Molecule("O2")))     << endl;
    cout << "CO2 == CO                    : "     << (IonSpecie(Molecule("CO2"))      == IonSpecie(Molecule("CO")))     << endl;
    cout << "CO2 == CO2                   : "     << (IonSpecie(Molecule("CO2"))      == IonSpecie(Molecule("CO2")))    << endl;
    cout << "HCOOH == CH2O2               : "     << (IonSpecie(Molecule("HCOOH"))    == IonSpecie(Molecule("CH2O2")))  << endl;
    cout << "Si(CH3)4 == Si(CH3)2(CH3)2   : "     << (IonSpecie(Molecule("Si(CH3)4")) == IonSpecie(Molecule("(CH3)4Si")))  << endl;
    cout << "Si(CH3)4 == (CH3)4Si         : "     << (IonSpecie(Molecule("Si(CH3)4")) == IonSpecie(Molecule("(CH3)4Si")))  << endl;
    cout << "Si(CH3)4 == Si(CH2)4         : "     << (IonSpecie(Molecule("Si(CH3)4")) == IonSpecie(Molecule("Si(CH2)4")))  << endl;
*/


    cout << "Parse DT          : "     << IonSpecie(Molecule("DT")).toString()  << endl;
    cout << "Parse DT2         : "     << IonSpecie(Molecule("DT2")).toString() << endl;

	vector<IntScatteringTable> tables =  parser.getIntScatteringTables(ElectronSpecie(), IonSpecie(Molecule("CO2")));
}
