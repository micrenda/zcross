#include "zcross/ZCross.hpp"
#include "zcross/ProcessCollisionType.hpp"
#include <algorithm>
#include <iostream>
#include <boost/units/io.hpp>
#include <boost/units/quantity.hpp>
#include <boost/units/systems/si.hpp>
#include <boost/units/systems/si/prefixes.hpp>
#include <boost/units/systems/si/codata/electromagnetic_constants.hpp>

using namespace dfpe;
using namespace std;
using namespace boost::units;

int main()
{
	ZCross parser;
	

	parser.loadDatabase("anu_differential");

	ProcessFilter filter;
	filter.addFilterByGroupId("HCOOH:2006");
	filter.addFilterByProcessCollisionType(ProcessCollisionType::ELASTIC);

	vector<DifScatteringTable> intTables =  parser.getDifScatteringTables(filter);

	//sort(tables.begin(), tables.end());
    const DifScatteringTable& table = intTables[0];


	cout << left
		 << setw (16) << table.getDatabaseId()
		 << setw (16) << table.getGroupId()
		 << setw (6) << table.getProcessId()
		 << setw (48) << table.getProcess().getComments()
		 << endl;
    cout << table.getTableContent(PrintMode::PRETTY) << endl;


}
