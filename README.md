# ZCross

## Introduction

`ZCross` is a C++ library to read cross section tables for electron and ion scattering processes.

If you use this project in your research, please consider citing [this article](http://www.nipne.ro/rjp/2019_64_9-10/RomJPhys.64.402.pdf) ([bibtex](doc/zcross.bib)):

> *ZCross: A Versatile Library to Manage Gas Scattering Cross-Section Table*  
> *Romanian Journal of Physics 64, 402 (2019)*

## How to install

First you have to clone [ZCross](https://gitlab.com/micrenda/zcross) to your pc:
``` bash
git clone https://gitlab.com/micrenda/zcross.git
```
Then you have to make sure you have the [boost](https://www.boost.org/) headers installed. For Debian/Ubuntu you can do something like:
``` bash
sudo apt install libboost-dev libboost-filesystem-dev libboost-program-options-dev libboost-regex-dev libboost-system-dev
```

Then you can proceed to compile and install:
``` bash
cd zcross
mkdir build && cd build
cmake CMAKE_INSTALL_PREFIX=/opt/zcross/ ..
make
sudo make install
```

If you are interested on the plotting utlities, please install the python binding:
``` bash
pip install zcross
```

**Be aware**: the compilation of `ZCross` requires the `C++17` standard. A recent Linux distribution (e.g. Debian 10, Ubuntu 19.10, CentOS 8, etc.) will likely provide a compiler with `C++17` support.

The last step consist to make the `ZCross` library available for linking. In a Debian system, it is possible to create the file `/etc/ld.so.conf.d/zcross.conf`
``` bash
/opt/zcross/lib
```
and execute:
``` bash
sudo ldconfig
```

## Getting the cross section databases

A complete set of cross-section databases can be downloaded from the [offline download section of LXCat](https://lxcat.net/data/download.php).
Save the cross section tables in any location (we suggest under `/opt/zcross_data`) and define an enviroment variable pointing to that path:
``` bash
export ZCROSS_DATA=/opt/zcross_data
```
(you can add it to your `.profile` file)

## How to use standalone client

First you have to make sure your `PATH` directory contains the zcross bin

``` bash
export PATH=$PATH:/opt/zcross/bin
```
(you can add it to your `.profile` file)

Now you can explore the cross sections in your system using the `zcross` command:
``` bash
zcross list --target 'Ar'      # List all the tables with a Argon target"
zcross list --bullet 'He^+'    # List all the tables with a Helium ion bullet"

zcross list 'Itikawa'          # List the tables of database Itikawa
zcross list 'Itikawa/N2'       # List the tables of the database Itikawa and group N2
zcross list 'Itikawa/N2/1-5'   # List the tables of the database Itikawa and group N2 with id between 1 and 5

zcross list '*/N2'             # List all the groups N2

zcross show 'emol-lehavre'     # Show the content of all tables of EMol-Lehavre

zcross plot 'biagi8/Ar'        # Plot the cross tables (requres the zcross python pacakge)
```

## Python interface

Another pure Python library is [avalilable](https://gitlab.com/micrenda/zcross-python) to read the cross section data. You can install it using pip:
``` bash
pip install zcross
```
It requires `Python 3`

## Project example
A stardard C++ project using `ZCross` contains these files:
``` shell
example1
  ├── CMakeLists.txt
  └── src
      └── main.cpp
```

The file `CMakeLists.txt`:

``` cmake
cmake_minimum_required(VERSION 3.0)
project(example1)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -pedantic" )

include_directories(include)

file(GLOB SOURCE_FILES src/*.cpp)
add_executable(example1 ${SOURCE_FILES})

find_package (ZCross REQUIRED HINTS /opt/zcross/share/zcross/cmake/ )
if (${ZCross_FOUND})
    target_link_libraries (example1 PUBLIC DFPE::zcross)
endif()
```

## Application examples

You can find some examples in the directory [examples](examples/):

[Example 1](examples/example1/doc/README.md) - List all cross section tables for a specific database

[Example 2](examples/example2/doc/README.md) - Printing the references of a table selection

[Example 3](examples/example3/doc/README.md) - Query the value of a specific table

## Support

Support and sugestions can be found in the [ZCross section](https://forum.cold-plasma.org/viewforum.php?f=8) at [Cold-Plasma Forum](https://forum.cold-plasma.org/) or contact us [by email](mailto:michele.renda@cern.ch)
