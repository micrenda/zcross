#include "zcross/ZCross.hpp"
#include <algorithm>
#include <numeric>
#include <iostream>
#include <unordered_set>
#include <locale>
#include <codecvt>
#include <boost/locale.hpp>
#include <boost/program_options.hpp>
#include <boost/algorithm/string/split.hpp> 
#include <boost/algorithm/string.hpp> 
#include <boost/units/systems/si/codata/electromagnetic_constants.hpp>
#include <boost/units/systems/si/prefixes.hpp>
#include <boost/units/cmath.hpp>
#include <zcross/ZCrossError.hpp>
#include <libgen.h>
#include <fstream>

using namespace dfpe;
using namespace std;
using namespace boost::units;

namespace po = boost::program_options;

string padRight(const string& source, int lenMin, char pad = ' ')
{
    string::const_iterator s = source.begin();
    int len = 0;
    while (*s) len += (*s++ & 0xc0) != 0x80;
    
    string result = source;
    
    for (int i = len; i < lenMin; i++)
        result += pad;
        
        
    return result;
    
}


vector<string> split(const string& s, char delimiter)
{
   vector<string> tokens;
   string token;
   istringstream tokenStream(s);
   while (getline(tokenStream, token, delimiter))
   {
      tokens.push_back(token);
   }
   return tokens;
}


string comment(const string& s, unsigned int max_size, bool verbose)
{
    string suffix = "... (use -v flag)";
    
    if (!verbose)
    {
        if (s.length() >= max_size)
        {
            return s.substr(0, max_size - suffix.length()) + suffix;
        }
    }
    
    return s;
    
}


int main(const int argc, const char *argv[])
{

    po::options_description desc(
    "ZCross is a command line tool to see the available cross section tables installed in your system.\n"
    "\n"    
    "Usage:\n"
    "  zcross list                # List all the tables available in your system\n"
    "  zcross list Biagi          # List all the tables of the Biagi database\n"
    "  zcross list Itikawa/CO2    # List all the tables of the Itikawa database and group CO2\n"
    "  zcross list Itikawa/CO2/1  # List the first table of the Itikawa database and group CO2\n"
    "  zcross list */CO2          # List all the tables of the group CO2 in all databases\n"
    "  \n"
    "  zcross list -b e -t CO2          # List the tables with an electron bullet and CO2 target\n"
    "  zcross list -c excitation_vib    # List the tables for virational excitation processes\n"
    "  \n"
    "  zcross show Phelps/N2            # Show the entries of the cross section tables matching the criteria\n"
    "  \n"
    "  zcross export Biagi8             # Save the tables in txt or csv format\n"
    "  \n"
    "  zcross ref Viehland              # Show all the references of the database Viehland\n"
    "  zcross ref Viehland/YangTT       # Show all the references of the database Viehland and group YangTT\n"
    "  \n"
    "  zcross fit Biagi/*               # Show the fit parameters for the selected tables\n"
    "  \n"
    "  zcross span --min 0.001 --max 30 Biagi/*     # Get the span value between 0.001 eV and 30 eV for the selected tables\n"
    "  \n"
    "  zcross plot Biagi/CO2/1-5  # Plots the first 5 tables of the Biagi database and group CO2\n"
    "  \n"
    "Allowed options"
    );
    
    desc.add_options()
            ("help,h", "produce help message")
            ("verbose,v", "print the full comment")
            ("bullet,b",    po::value<string>(), "filter by bullet")
            ("target,t",    po::value<string>(), "filter by target")
            ("collision,c",    po::value<string>(), "filter by collision type")
            ("velocity",  "show bullet velocity instead of energy")
            ("exact,e",   "when searching for a bullet/target, show only exact results (same ionization and state)")
            ("csv",    "CSV format")
            ("output,o",    "Output file")
            ("min",    po::value<double>(), "For span command: minimum energy to calculate the span")
            ("max",    po::value<double>(), "For span command: maximum energy to calculate the span")
            ("bibtex", "Show references in bibtex format")
            ("show-comments",    "show comments");
            
    po::options_description hidden;
    
    hidden.add_options()
            ("command", po::value<string>()/*->required()*/, "command")
            ("tables",  po::value<vector<string>>()->multitoken(), "tables to show");

    po::options_description all_options;
    all_options.add(desc);
    all_options.add(hidden);

    po::positional_options_description p;
    p.add("command",  1);
    p.add("tables",  -1);

    quantity<si::energy> eV = ZCrossTypes::electronvoltEnergy;
    quantity<si::area>   m2(1. * si::meter * si::meter);
    quantity<si::velocity> cm_us(1. * si::centi * si::meter / (si::micro * si::second));

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(all_options).positional(p).allow_unregistered().run(), vm);
    po::notify(vm);

    bool verbose = false;
    
    if (vm.count("help"))
    {
        cout << desc << "\n";
        return 1;
    }
    
    if (vm.count("verbose"))
    {
        verbose = true;
    }

    string command;

    if (vm.count("command"))
    {
        command = vm["command"].as<string>();
    }

    bool csv = false;
    if (vm.count("csv"))
        csv = true;

    bool bibtex = false;
    if (vm.count("bibtex"))
        bibtex = true;

    bool velocity = false;
    if (vm.count("velocity"))
        velocity = true;

    bool exact = false;
    if (vm.count("exact"))
        exact = true;

    std::optional<QtySiEnergy> spanMin;
    std::optional<QtySiEnergy> spanMax;

    if (vm.count("min"))
        spanMin = vm["min"].as<double>() * eV;
    if (vm.count("max"))
        spanMax = vm["max"].as<double>() * eV;

    string red = "\u001B[31m";
    string blu = "\u001B[34m";
    string gry = "\u001B[37m";
    string rst = "\u001B[0m";

    if (!isatty(fileno(stdout)))
    {
        // removing ANSI codes when redirecting output to something that is not a terminal
        red = "";
        blu = "";
        gry = "";
        rst = "";
    }

    ProcessFilterAny filters;
    vector<string> tables = {"*"};

    if (vm.count("tables"))
        tables = vm["tables"].as<vector<string>>();
        
    for (string table: tables)
    {
        ProcessFilter filter(table, exact);

        if (vm.count("bullet"))
        {
            Specie bullet(vm["bullet"].as<string>());
            filter.addFilterByBullet(bullet);
        }

        if (vm.count("target"))
        {
            Specie target(vm["target"].as<string>());
            filter.addFilterByTarget(target);
        }
        
        if (vm.count("collision"))
        {
            string collision = vm["collision"].as<string>();
            
            if (collision  == "elastic")
                filter.addFilterByProcessCollisionType(ProcessCollisionType::ELASTIC);
            else if (collision == "superelastic")
                filter.addFilterByProcessCollisionType(ProcessCollisionType::SUPERELASTIC);
            else if (collision == "total")
                filter.addFilterByProcessCollisionType(ProcessCollisionType::TOTAL);
            else if (collision == "inelastic")
                filter.addFilterByProcessCollisionType(ProcessCollisionType::INELASTIC);
            else if (collision == "excitation")
            {
                filter.addFilterByProcessCollisionType(ProcessCollisionType::INELASTIC);
                filter.addFilterByProcessCollisionInelasticTypes({
                    ProcessCollisionInelasticType::EXCITATION_ELE,
                    ProcessCollisionInelasticType::EXCITATION_VIB,
                    ProcessCollisionInelasticType::EXCITATION_ROT
                });
            }
            else
            {
                filter.addFilterByProcessCollisionType(ProcessCollisionType::INELASTIC);
                filter.addFilterByProcessCollisionInelasticType(ProcessCollisionInelasticUtil::parse(collision));
            }
        }
        filters.addFilter(filter);
    }



	if (command != "list" && command != "show"  && command != "ref" && command != "plot" && command != "fit" && command != "span" && command != "export" )
	{
		cout << "Invalid command: '" << command << "'. Use the -h flag to get a list of valid arguments.";
		return 1;
	}
	
	if (command != "plot"  )
	{
			ZCross parser;
			
			bool allDatabasesFlag    = false;
			set<string> databases;
			
			for (const ProcessFilter& filter: filters.getFilters())
			{       
					if (filter.hasFilterByDatabaseId())
					{
							const vector<string> filtersDatabaseId = filter.getFilterByDatabaseId();
							databases.insert(filtersDatabaseId.begin(), filtersDatabaseId.end());
					}
					else
					{
							allDatabasesFlag = true;
							break;
					}
			}
			
			if (allDatabasesFlag || databases.empty())
					parser.loadDatabases();
			else
					parser.loadDatabases(vector<string>(databases.begin(), databases.end()));
			
			
			vector<IntScatteringTable> intTables =  parser.getIntScatteringTables(filters);
			vector<DifScatteringTable> difTables =  parser.getDifScatteringTables(filters);

			sort(intTables.begin(), intTables.end());
			sort(difTables.begin(), difTables.end());


			if (command == "list")
			{
					
					if (!csv)
					{
							 cout << left << red
											 << setw(24) << "TABLE"
											 << setw(16) << "COLLISION"
											 << setw(14) << "MOMENT ORDER"
											 << setw(14) << "THRESHOLD"
											 << setw(48) << "REACTION"
											 /*<< setw(60)*/ << "COMMENT"
											 << rst << endl;
							
							for (const IntScatteringTable &table: intTables)
                            {
							    string momentOrder = "";
                                if (table.getProcess().getCollisionType() == ProcessCollisionType::ELASTIC)
                                {
                                    momentOrder = to_string(table.getProcess().getMomentOrder());
                                    momentOrder.insert(momentOrder.begin(), 4 - momentOrder.size(), ' ');
                                    momentOrder +=  + " " + ProcessMomentOrderUtil::getDesc(table.getProcess().getMomentOrder());
                                }

                                std::optional<QtySiEnergy> threshold = table.getEnergyThreshold();

                                cout << left
                                     << setw(24) << table.getId().toString()
                                     << setw(16) << table.getProcess().getCompactDescription()
                                     << setw(14) << momentOrder
                                     << setw(14)  << std::right << (threshold.has_value() ? to_string((double) (threshold.value() / eV)) + " eV " : "") << std::left
                                     << setw(48) << padRight(table.getProcess().getReaction().toString(PrintMode::PRETTY), 48)
                                     /*<< setw(60)*/ << comment(table.getProcess().getComments(), 60, verbose)
                                     << endl;
                            }
							for (const DifScatteringTable &table: difTables)
                            {
                                string momentOrder = "";
                                if (table.getProcess().getCollisionType() == ProcessCollisionType::ELASTIC)
                                {
                                    momentOrder = to_string(table.getProcess().getMomentOrder());
                                    momentOrder.insert(momentOrder.begin(), 4 - momentOrder.size(), ' ');
                                    momentOrder +=  + " " + ProcessMomentOrderUtil::getDesc(table.getProcess().getMomentOrder());
                                }

                                std::optional<QtySiEnergy> threshold = table.getEnergyThreshold();

                                cout << left
                                     << setw(16) << table.getDatabaseId()
                                     << setw(16) << table.getGroupId()
                                     << setw(6) << table.getProcessId()
                                     << setw(16) << table.getProcess().getCompactDescription()
                                     << setw(16) << ProcessDirectionUtil::getNameDefault(table.getProcess().getDirectionType(), "")
                                     << setw(14) << momentOrder
                                     << setw(14) << std::right << (threshold.has_value() ? to_string((double) (threshold.value() / eV)) + " eV " : "") << std::left
                                     << setw(48) << table.getProcess().getReaction().toString(PrintMode::PRETTY)
                                     /*<< setw(60)*/ << comment(table.getProcess().getComments(), 60, verbose)
                                     << endl;
                            }
					}
					else
					{

					cout     << "DATABASE,"
							 << "GROUP,"
							 << "ID,"
							 << "COLLISION,"
							 << "DIRECTION,"
							 << "MOMENT_ORDER,"
							 << "THRESHOLD [eV],"
							 << "DISSOCIATIVE,"
							 << "REACTION,"
							 << "COMMENT"
							 << endl;
											 
							for (const IntScatteringTable &table: intTables)
                            {
                                std::optional<QtySiEnergy> threshold = table.getEnergyThreshold();

                                cout << left
                                     << table.getDatabaseId() << ","
                                     << table.getGroupId() << ","
                                     << table.getProcessId() << ","
                                     << table.getProcess().getCompactDescription() << ","
                                     << ProcessDirectionUtil::getNameDefault(table.getProcess().getDirectionType(), "") << ","
                                     << table.getProcess().getMomentOrder() << ","
                                     << (threshold.has_value() ? to_string((double) (threshold.value() / eV)): "") << ","
                                     << (table.getProcess().isDissociativeFlag() ? "Y" : "N") << ","
                                     << "\"" << table.getProcess().getReaction().toString(PrintMode::BASIC) << "\","
                                     << "\"" << table.getProcess().getComments() << "\"" << endl;
                            }

							for (const DifScatteringTable &table: difTables)
                            {
                                std::optional<QtySiEnergy> threshold = table.getEnergyThreshold();
                                cout << left
                                     << table.getDatabaseId() << ","
                                     << table.getGroupId() << ","
                                     << table.getProcessId() << ","
                                     << table.getProcess().getCompactDescription() << ","
                                     << ProcessDirectionUtil::getNameDefault(table.getProcess().getDirectionType(), "") << ","
                                     << table.getProcess().getMomentOrder() << ","
                                     << (threshold.has_value() ? to_string((double) (threshold.value() / eV)): "") << ","
                                     << (table.getProcess().isDissociativeFlag() ? "Y" : "N") << ","
                                     << "\"" << table.getProcess().getReaction().toString(PrintMode::BASIC) << "\","
                                     << "\"" << table.getProcess().getComments() << "\"" << endl;

                            }
					}
			}
			else if (command == "show")
			{
					if (!csv)
					{
							for (const IntScatteringTable& table: intTables)
							{
                                    std::optional<QtySiEnergy> threshold = table.getEnergyThreshold();

									cout << "╭──────────────────────────────────────────────────────────────────────╮" << endl;
									cout << "│" + blu + padRight(table.getId().toString(PrintMode::PRETTY), 70) + rst << "│"  << endl;
									cout << "╰──────────────────────────────────────────────────────────────────────╯" << endl;
																	
									cout << red << setw(16) << "TYPE"     << rst << ": " << TableTypeUtil::getName(table.getProcessTableType()) << endl;
									cout << red << setw(16) << "PROCESS"  << rst << ": " << table.getProcess().getShortDescription() << endl;
									cout << red << setw(16) << "REACTION" << rst << ": " << table.getProcess().getReaction().toString(PrintMode::PRETTY) << endl;
									cout << red << setw(16) << "MOMENT ORDER" << rst << ": " << table.getProcess().getMomentOrder() << endl;
                                    cout << red << setw(16) << "THRESHOLD"    << rst << ": " << (threshold.has_value() ? to_string((double) (threshold.value() / eV)) + " eV": "") << endl;
									cout << red << setw(16) << "DISSOCIATIVE" << rst << ": " << (table.getProcess().isDissociativeFlag() ? "Yes" : "No") << endl;
									cout << endl;
									
									quantity<si::mass> bulletMass = table.getProcess().getReaction().getReactants().getBullet().getMass();
									
									if (!velocity)
													cout << right << red
															 << setw(16) << "ENERGY"
															 << "   "
															 << setw(16) << "CROSS SECTION"
															 << rst << endl;
											else
													cout << right << red
															 << setw(16) << "VELOCITY"
															 << "   "
															 << setw(16) << "CROSS SECTION"
															 << rst << endl;
									
									
									for (const auto &pair : table.getTable())
									{
											if (!velocity)
													cout << right
															 << setw(16) << (double) (pair.first / eV)  << " eV"
															 << setw(16) << (double) (pair.second / m2) << " m²"
															 << endl;
											else
													cout << right
															 << setw(16) << (double) (sqrt(2.* pair.first / bulletMass ) / cm_us) << " cm/µs"
															 << setw(16) << (double) (pair.second / m2) << " m²"
															 << endl;
									}

									// TODO: Show PARAMETERS
									
									if (!table.getProcess().getComments().empty())
									{
											cout << endl;
											cout << red << "COMMENTS: " << rst << endl;
											cout << table.getProcess().getComments() << endl;
									}

									vector<Reference> processRefs = parser.getProcessReferences(table);
									if(!processRefs.empty())
									{
											cout << endl;
											cout << red << "PROCESS REFERENCES:" << rst << endl;
											for (const Reference &reference: processRefs)
													cout << reference.toString(bibtex?PrintMode::LATEX:PrintMode::PRETTY) << endl;
									}
									
									vector<Reference> groupRefs = parser.getGroupReferences(table);
									if (!groupRefs.empty())
									{
											cout << endl;
											cout << red << "GROUP REFERENCES:" << rst << endl;
											for (const Reference &reference: groupRefs)
													cout << reference.toString(bibtex?PrintMode::LATEX:PrintMode::PRETTY) << endl;
									}
															  
									vector<Reference> databaseRefs = parser.getDatabaseReferences(table);
									if (!databaseRefs.empty())
									{
											cout << endl;
											cout << red << "DATABASE REFERENCES:" << rst << endl;
											for (const Reference &reference: databaseRefs)
													cout << reference.toString(bibtex?PrintMode::LATEX:PrintMode::PRETTY) << endl;
									}
									
							}
					}
					else
                    {

                    }
			}
			else if (command == "ref")
			{
					
					unordered_set<Reference> refs;
					
					for (const IntScatteringTable& table: intTables)
					{
							vector<Reference> processRefs = parser.getProcessReferences(table);
							if(!processRefs.empty())
							{
									for (const Reference &reference: processRefs)
											refs.insert(reference);
							}
							
							vector<Reference> groupRefs = parser.getGroupReferences(table);
							if (!groupRefs.empty())
							{
									for (const Reference &reference: groupRefs)
											refs.insert(reference);
							}
													  
							vector<Reference> databaseRefs = parser.getDatabaseReferences(table);
							if (!databaseRefs.empty())
							{
									for (const Reference &reference: databaseRefs)
											refs.insert(reference);
							}
					}
					
					if (!csv)
					{   
							for (const Reference& ref: refs)
									cout << ref.toString(bibtex?PrintMode::LATEX:PrintMode::PRETTY) << endl;
					}
					else
					{
							set<string> fields;
							
							for (const Reference& ref: refs)
							{
									const vector<string>& refFields = ref.getFields();
									fields.insert(refFields.begin(), refFields.end());
							}
							
							cout << "LABEL,";
							cout << " TYPE,";
							for (const string& field: fields)
							{
									string header = field;
									transform(header.begin(), header.end(),header.begin(), ::toupper);
									cout << " " << header << ",";
							}
							cout << endl;
							
							for (const Reference& ref: refs)
							{
									cout << ref.getLabel() << ",";
									cout << " " << ref.getType() << ",";
									
									for (const string& field: fields)
									{
											if (ref.hasField(field))
													cout << " \"" << ref.getValue(field) << "\",";
											else
													cout << " \"\",";
									}
									cout << endl;
							}
							
					}
			}
            else if (command == "fit" || command == "span")
            {
                if (!csv)
                {
                    cout << left << red
                         << setw(24) << "TABLE"
                         << setw(16) << "COLLISION"
                         << setw(13) << "MOMENT ORDER"
                         << setw(40) << "REACTION"

                         << right << setw(16) << "LOWER ENERGY"
                         << right << setw(16) << "UPPER ENERGY";

                    if (command == "fit")
                        cout
                         << right << setw(16) << "LOWER FIT A"
                         << right << setw(16) << "LOWER FIT K"
                         << right << setw(16) << "UPPER FIT A"
                         << right << setw(16) << "UPPER FIT K";
                    else if (command == "span")
                        cout
                         << right << setw(16) << "SPAN";

                    cout
                         << rst << endl;

                    for (const IntScatteringTable &table: intTables)
                    {
                        QtySiEnergy lower = spanMin.has_value() ? max(spanMin.value(), table.getEnergyLowerBound()) : table.getEnergyLowerBound();
                        QtySiEnergy upper = spanMax.has_value() ? min(spanMax.value(), table.getEnergyUpperBound()) : table.getEnergyUpperBound();
                        cout << left
                             << setw(24) << table.getId().toString()
                             << setw(16) << table.getProcess().getCompactDescription()
                             << right << setw(6) << table.getProcess().getMomentOrder() << left
                             << setw(7) << " " + ProcessMomentOrderUtil::getDesc(table.getProcess().getMomentOrder())
                             << setw(40) << padRight(table.getProcess().getReaction().toString(PrintMode::PRETTY), 40)
                             << right << setw(13) << (double) (lower/eV) << left << " eV"
                             << right << setw(13) << (double) (upper/eV) << left << " eV";

                        if (command == "fit")
                            cout
                             << right << setw(13) << (double) (table.getFitLowerA() / m2) << left << " m²"
                             << right << setw(16) << (double) table.getFitLowerK() << left
                             << right << setw(13) << (double) (table.getFitUpperA() / m2) << left << " m²"
                             << right << setw(16) << (double) table.getFitUpperK() << left;
                        else if (command == "span")
                            cout
                             << right << setw(13) << (double) (table.getSpan(lower, upper) / QtySiArea(1. * si::meter * si::meter)) << left << " m²";

                        cout
                             << endl;
                    }
                }
                else
                {
                    cout << "TABLE,"
                         << "COLLISION,"
                         << "MOMENT_ORDER,"
                         << "REACTION,"
                         << "LOWER_ENERGY [eV],"
                         << "UPPER_ENERGY [eV],";

                    if (command == "fit")
                        cout
                         << "LOWER_FIT_A  [m^2],"
                         << "LOWER_FIT_K,"
                         << "UPPER_FIT_A  [m^2],"
                         << "UPPER_FIT_K";
                    else if (command == "span")
                        cout
                         << "SPAN [m^2]";

                    cout << endl;

                    for (const IntScatteringTable &table: intTables)
                    {
                        QtySiEnergy lower = spanMin.has_value() ? max(spanMin.value(), table.getEnergyLowerBound()) : table.getEnergyLowerBound();
                        QtySiEnergy upper = spanMax.has_value() ? min(spanMax.value(), table.getEnergyUpperBound()) : table.getEnergyUpperBound();

                        cout << table.getId().toString() << ","
                             << table.getProcess().getCompactDescription() << ","
                             << table.getProcess().getMomentOrder() << ","
                             << "\"" << table.getProcess().getReaction().toString(PrintMode::BASIC)   << "\","
                             << (double) (lower/eV) << ","
                             << (double) (upper/eV) << ",";

                        if (command == "fit")
                            cout
                             << (double) (table.getFitLowerA() / m2)   << ","
                             << (double)  table.getFitLowerK()         << ","
                             << (double) (table.getFitUpperA() / m2)   << ","
                             << (double)  table.getFitUpperK();
                        else if (command == "span")
                            cout
                             << (double) (table.getSpan(lower, upper) / QtySiArea(1. * si::meter * si::meter));

                        cout << endl;
                    }
                }
            }
            else if (command == "export")
            {
                if (!csv)
                {
                    for (const IntScatteringTable &table: intTables)
                    {

                        const TableId &id      = table.getId();

                        string        filename = id.getDatabaseId() + "_" + id.getGroupId() + '_' + to_string(id.getProcessId()) + ".txt";
                        ofstream ofs;
                        ofs.open(filename, ios::out);

                        QtySiMass bulletMass = table.getProcess().getReaction().getReactants().getBullet().getMass();

                        ofs << "# " << left << setw(20) << "DATABASE: "     << id.getDatabaseId() << endl;
                        ofs << "# " << left << setw(20) << "GROUP: "        << id.getGroupId()    << endl;
                        ofs << "# " << left << setw(20) << "PROCESS: "      << id.getProcessId()  << endl;
                        ofs << "# " << endl;
                        ofs << "# " << left << setw(20) << "TYPE:"          << TableTypeUtil::getName(table.getProcessTableType()) << endl;
                        ofs << "# " << left << setw(20) << "PROCESS:"       << table.getProcess().getShortDescription() << endl;
                        ofs << "# " << left << setw(20) << "REACTION:"      << table.getProcess().getReaction().toString(PrintMode::PRETTY) << endl;
                        ofs << "# " << left << setw(20) << "MOMENT ORDER:"  << table.getProcess().getMomentOrder() << endl;
                        ofs << "# " << left << setw(20) << "THRESHOLD:"     << (double) (table.getEnergyThreshold() / eV) << " eV" << endl;
                        ofs << "# " << left << setw(20) << "DISSOCIATIVE:"  << (table.getProcess().isDissociativeFlag() ? "Yes" : "No") << endl;

                        //TODO: Show parameters

                        if (!table.getProcess().getComments().empty())
                        {
                            std::stringstream commentStream(table.getProcess().getComments());
                            std::string line;

                            ofs << "# " << endl;
                            ofs << "# " << left << setw(20) << "COMMENTS:" << endl;

                            while(std::getline(commentStream,line,'\n'))
                                ofs << "# " << line << endl;
                        }

                        ofs << "# " << endl;

                        if (!velocity)
                            ofs << "# ENERGY [eV]\tAREA [m²]" << endl;
                        else
                            ofs << "# VELOCITY [cm/µs]\tAREA [m²]" << endl;

                        for (const auto &pair : table.getTable())
                        {
                            if (!velocity)
                            {
                                ofs << (double) (pair.first / eV) << "\t\t"
                                    << (double) (pair.second / m2) << endl;
                            }
                            else
                            {
                                ofs << (double) (sqrt(2. * pair.first / bulletMass) / cm_us) << "\t\t"
                                    << (double) (pair.second / m2) << endl;
                            }
                        }

                        ofs.close();

                        cout << "Exported table " << id.toString() << " to " << filename << endl;
                    }
                }
                else
                {
                    for (const IntScatteringTable &table: intTables)
                    {

                        const TableId &id = table.getId();
                        string filename = id.getDatabaseId() + "_" + id.getGroupId() + '_' + to_string(id.getProcessId()) + ".csv";

                        ofstream ofs;
                        ofs.open(filename, ios::out);

                        QtySiMass bulletMass = table.getProcess().getReaction().getReactants().getBullet().getMass();

                        ofs << "ENERGY [eV],AREA [m^2]" << endl;

                        for (const auto &pair : table.getTable())
                        {
                            if (!velocity)
                                ofs  << (double) (pair.first  / eV) << ","
                                     << (double) (pair.second / m2) << endl;
                            else
                                cout << (double) (sqrt(2. * pair.first / bulletMass) / cm_us) << ","
                                     << (double) (pair.second / m2) << endl;
                        }
                        ofs.close();

                        cout << "Exported table " << id.toString() << " to " << filename << endl;
                    }
                }
            }
			else
			{
				cout << "Invalid command: '" << command << "'. Use the -h flag to get a list of valid arguments.";
				return 1;
			}
	}
    else
    {
        string plot_exe = "zcross-plot";

        string lcommand;
        lcommand += plot_exe;

        for (int i = 2; i < argc; i++)
            lcommand += " \"" + string(argv[i]) + "\"";

        system(lcommand.c_str());

    }
}
