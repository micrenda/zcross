#!/usr/bin/env bash

markdown-pp README.mdpp -o README.md

pushd examples
pwd
ls -d * | while read DIR
do
	echo "Entering $DIR"
	pushd $DIR/doc
	markdown-pp README.mdpp -o README.md
	popd
done
popd
