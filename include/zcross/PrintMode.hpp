#pragma once

#include <string>

namespace dfpe
{
    enum class PrintMode
    {
        BASIC, PRETTY, LATEX
    };
}
