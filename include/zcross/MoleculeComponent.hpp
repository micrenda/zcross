#pragma once
#include "Element.hpp"

namespace dfpe
{
    class Molecule;


    class MoleculeComponent
    {
    public:
        MoleculeComponent(const MoleculeComponent  &other);
        MoleculeComponent(const Element  &element);
        MoleculeComponent(const Molecule &molecule);

        virtual ~MoleculeComponent();

    public:
        bool isElement() const;
        bool isMolecule() const;

        Element getElement() const;
        Molecule getMolecule() const;

    private:
        bool  simple;
        void* component = nullptr; // Pointer to Element or Molecule

    public:
        bool operator==(const MoleculeComponent &rhs) const;
        bool operator!=(const MoleculeComponent &rhs) const;
        MoleculeComponent& operator=(const MoleculeComponent &rhs);
    };

}

