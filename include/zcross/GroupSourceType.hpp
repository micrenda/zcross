#pragma once

#include <string>

namespace dfpe
{
    enum class GroupSourceType
    {
       EXPERIMENT,THEORY,MIXED
    };

    class GroupSourceUtil
    {
    public:
        static const std::string        getName(const GroupSourceType& type);
        static const std::string        getDesc(const GroupSourceType& type);
        static GroupSourceType     		parse(const std::string& s);
    };
}
