#pragma once

#include <boost/units/dimension.hpp>
#include <boost/units/base_dimension.hpp>
#include <boost/units/derived_dimension.hpp>
#include <boost/units/base_unit.hpp>

#include <boost/units/systems/si/length.hpp>
#include <boost/units/systems/si/time.hpp>
#include <boost/units/make_system.hpp>
#include <boost/units/io.hpp>

#include <qtydef/QtyDefinitions.hpp>

namespace dfpe
{
	/// derived dimension for number area/sr in SI units : L²SolidAng⁻¹
	typedef boost::units::make_dimension_list<boost::mpl::list<
			boost::units::dim<boost::units::length_base_dimension,    	   boost::units::static_rational<2>>,
			boost::units::dim<boost::units::solid_angle_base_dimension,    boost::units::static_rational<-1>>
	>>::type     area_per_solidangle_dimension;
	
	/// derived dimension for number density in SI units : L⁻³
    typedef boost::units::make_dimension_list<boost::mpl::list<
            boost::units::dim<boost::units::length_base_dimension,    boost::units::static_rational<-3>>
    >>::type     density_dimension;

    /// derived dimension for molar density in SI units : N L⁻³
    typedef boost::units::make_dimension_list<boost::mpl::list<
            boost::units::dim<boost::units::amount_base_dimension,    boost::units::static_rational<1>>,
            boost::units::dim<boost::units::length_base_dimension,    boost::units::static_rational<-3>>
    >>::type     molar_density_dimension;
	
	typedef boost::units::unit<area_per_solidangle_dimension, boost::units::si::system>		si_area_per_solidangle;
	typedef boost::units::unit<density_dimension, boost::units::si::system>                 si_density;
    typedef boost::units::unit<molar_density_dimension, boost::units::si::system>           si_molar_density;

	typedef boost::units::quantity<si_area_per_solidangle> QtySiAreaPerSolidAngle;
	typedef boost::units::quantity<si_density>             QtySiDensity;
    typedef boost::units::quantity<si_molar_density>       QtySiMolarDensity;
	    
    
    class ZCrossTypes
    {
    public:
        // Useful contants
        static QtySiEnergy electronvoltEnergy;
    };
}
