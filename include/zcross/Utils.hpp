#pragma once

#include <boost/regex.hpp>
#include <string>

namespace dfpe
{
    class Utils
    {
    public:
        static const std::string formatElementPretty(const boost::smatch &what);
        static const std::string formatElementLatex(const boost::smatch &what);
        static const std::string getScriptedNumber(int number, bool superscript, bool forcePlus = false);

    public:
        static const boost::regex specieRegex;
        static const boost::regex moleculeRegex;
        static const boost::regex elementRegex;
        static const boost::regex isomerRegex;


    };
}

