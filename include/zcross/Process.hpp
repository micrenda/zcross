#pragma once

#include "ProcessCollisionType.hpp"
#include "ProcessCollisionInelasticType.hpp"
#include "ProcessDirectionType.hpp"
#include "ProcessMomentOrder.hpp"
#include "Reaction.hpp"
#include <any>

namespace dfpe
{
    class Process
    {
    protected:
        ProcessCollisionType	        collisionType;
        ProcessCollisionInelasticType  	collisionInelasticType;
        ProcessDirectionType            directionType;
        int            					momentOrder;
        bool                            dissociativeFlag;
        

        Reaction 		reaction;
        std::string 	comments;
        
        std::map<std::string, std::any> parameters;


    public:
    
        ProcessCollisionType getCollisionType() const
        {
            return collisionType;
        }

        ProcessCollisionInelasticType getCollisionInelasticType() const
        {
            return collisionInelasticType;
        }

        ProcessDirectionType getDirectionType() const
        {
            return directionType;
        }
        
        int getMomentOrder() const
        {
            return momentOrder;
        }

        bool isDissociativeFlag() const
        {
            return dissociativeFlag;
        }

        const Reaction &getReaction() const
        {
            return reaction;
        }

        const std::string &getComments() const
        {
            return comments;
        }

        std::string getShortDescription() const;
        std::string getCompactDescription() const;
        std::string getVeryShortDescription() const;
        std::string getShortestDescription() const;


        void setCollisionType(ProcessCollisionType collisionType)
        {
            Process::collisionType = collisionType;
        }

        void setCollisionInelasticType(ProcessCollisionInelasticType collisionInelasticType)
        {
            Process::collisionInelasticType = collisionInelasticType;
        }

        void setDirectionType(ProcessDirectionType directionType)
        {
            Process::directionType = directionType;
        }

        void setMomentOrder(int momentOrder)
        {
            Process::momentOrder = momentOrder;
        }

        void setDissociativeFlag(bool dissociativeFlag)
        {
            Process::dissociativeFlag = dissociativeFlag;
        }

        void setReaction(const Reaction &reaction)
        {
            Process::reaction = reaction;
        }

        void setComments(const std::string &comments)
        {
            Process::comments = comments;
        }


        template<typename T>
        void setParameter(const std::string& name, const T& value)
        {
            parameters[name] = value;
        }

        template<typename T>
        T getParameter(const std::string& name) const
        {
            return std::any_cast<T>(parameters.at(name));
        }

        template<typename T>
        bool hasParameter(const std::string& name) const
        {
            try
            {
                std::any_cast<T>(parameters.at(name));
                return true;
            }
            catch(const std::out_of_range& e)
            {
                return false;
            }
            catch (const std::bad_any_cast& e)
            {
                return false;
            }

        }

        void setParameters(const std::map<std::string, std::any> &parameters)
        {
            Process::parameters = parameters;
        }

        const std::map<std::string, std::any> &getParameters() const
        {
            return parameters;
        }


    };

}
