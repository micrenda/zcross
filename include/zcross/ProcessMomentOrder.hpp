#pragma once

#include <string>

namespace dfpe
{
    enum class ProcessMomentOrderType
    {
        EL, MT, VS
    };

    class ProcessMomentOrderUtil
    {
    public:
        static const std::string   getName(int type);
        static const std::string   getDesc(int type);
        static int				   getValue(const ProcessMomentOrderType& type);
        static int   parse(const std::string& s);
    };
}
