#pragma once

#include <stdexcept>
#include <string>
#include <sstream>
#include <vector>

namespace dfpe
{
	class ZCrossErrorCls : public std::runtime_error
	{
	private:\
		std::string msg;

	public:
		ZCrossErrorCls(const std::string &arg, const char *file, int line): 
			std::runtime_error(arg)
		{
			msg = std::string(file) + ":" + std::to_string(line) + ": " + std::string(arg);
		}
		
		ZCrossErrorCls(const std::string &arg, const std::exception& parent, const char *file, int line):
			ZCrossErrorCls(arg, file, line)
		{
		    msg += "\n" + std::string(parent.what());
		}
		
		~ZCrossErrorCls() throw() {}

        const std::string &get_msg() const {return msg;}
        const char *what() const throw() { return msg.c_str();}
	};
	
	typedef ZCrossErrorCls ZCrossError;
}

#define ZCrossError(arg)            dfpe::ZCrossErrorCls(arg, __FILE__, __LINE__);
#define ZCrossErrorFwd(arg, parent) dfpe::ZCrossErrorCls(arg, parent, __FILE__, __LINE__);
