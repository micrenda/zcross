#pragma once
#include "ZCrossTypes.hpp"
#include <map>
#include "OutOfTableMode.hpp"
#include "BaseTable.hpp"
#include "PrintMode.hpp"

namespace dfpe
{
    class IntScatteringTable : public BaseTable
    {
	public:
	
		IntScatteringTable() = default;
        IntScatteringTable(const IntScatteringTable& other) = default;
		virtual ~IntScatteringTable() = default;
	
        void setTable(const std::map<QtySiEnergy, QtySiArea>& table);
        
        virtual unsigned int getSize() const override;
        virtual QtySiEnergy getEnergyLowerBound() const override;
		virtual QtySiEnergy getEnergyUpperBound() const override;

        virtual QtySiEnergy getEnergyThreshold() const override;

        virtual std::string getTableContent(const PrintMode& mode = PrintMode::BASIC) const override;

        QtySiArea getInterpoledValue(
			const QtySiEnergy& energy, 
			const OutOfTableMode& lowerBoundaryMode = OutOfTableMode::LAUNCH_EXCEPTION, 
			const OutOfTableMode& upperBoundaryMode = OutOfTableMode::LAUNCH_EXCEPTION) const;

		const std::map<QtySiEnergy, QtySiArea>
			getTable() const { return table; };

        TableType getProcessTableType() const override;

        /**
         * Given an integral cross-section tables, the spam is the integral between lower and upper of the cross-section
         * area over the log10 of the energy.
         *
         * This measure has not any physical meaning, but it may be usefull when compairing two cross-section tables to
         * check their coverage. For example, given a fixed lower and upper energy, a total cross section should have
         * the span equal to the span of the component cross sections.
         *
         * This quantity can also compare two equals cross section also if they have different energies (but it does not compare the shape, just the integral).
         *
         * Span(σ(ε)) = ∫ σ(ε) d log₁₀(ε)
         *
         * @param lower
         * @param upper
         * @return
         */
        QtySiArea getSpan(const QtySiEnergy& lower, const QtySiEnergy& upper) const;

        QtySiDimensionless  getFitLowerK() const { return fitLowerK; };
        QtySiArea           getFitLowerA() const { return fitLowerA; };
        QtySiDimensionless  getFitUpperK() const { return fitUpperK; };
        QtySiArea           getFitUpperA() const { return fitUpperA; };

	protected:
		std::map<QtySiEnergy, QtySiArea> table;

		mutable std::optional<QtySiEnergy> energyThreshold;

		QtySiDimensionless  fitLowerK;
		QtySiArea           fitLowerA;

		QtySiDimensionless  fitUpperK;
		QtySiArea           fitUpperA;
    };
}

