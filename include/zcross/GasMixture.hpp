/*
 * Copyright (c) 2018, Michele Renda, Dan Andrei Ciubotaru. All rights reserved.  
 * Licensed under the LGPL V3 License. See LICENSE file in the project root for full license information.  
 */
 
#pragma once

#include <zcross.hpp>
#include "ZCrossTypes.hpp"
#include <unordered_map>

namespace dfpe
{
    class GasMixture
    {
        public:

        void addComponent(const std::string &molecule, const QtySiDensity &numberDensity);
        void addComponent(const std::string &molecule, const QtySiMassDensity &massDensity);
        void addComponent(const std::string &molecule, const QtySiMolarDensity &molarDensity);

        void addComponent(const Molecule &molecule, const QtySiDensity &numberDensity);
        void addComponent(const Molecule &molecule, const QtySiMassDensity &massDensity);
        void addComponent(const Molecule &molecule, const QtySiMolarDensity &molarDensity);

        QtySiMassDensity   getComponentMassDensity  (const Molecule &molecule) const;
        QtySiDensity       getComponentDensity(const Molecule &molecule) const;
        QtySiMolarDensity  getComponentMolarDensity (const Molecule &molecule) const;


        std::vector<Molecule> getComponents() const;
        
        unsigned int size()  const;
        bool         empty() const;

        void clear();

    protected:
        std::unordered_map<Molecule, QtySiDensity>  numberDensities;

        QtySiTemperature temperature = QtySiTemperature(293.15 * boost::units::si::kelvins);

    public:
        void setTemperature(QtySiTemperature temperature) { GasMixture::temperature = temperature; };

        const QtySiTemperature& getTemperature() const { return temperature; };
        const QtySiPressure&    getPressure()    const;

    };

}
