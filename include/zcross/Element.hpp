
#pragma once

#include "ZCrossTypes.hpp"
#include <string>

namespace dfpe
{
    class Element
    {
    public:
        Element(const unsigned int z,
                const std::string &symbol,
                const std::string &name,
                const QtySiMass &mass) :
                z(z),
                symbol(symbol),
                name(name),
                mass(mass)
        {}
        
        Element():
				z(0),
                symbol(""),
                name(""),
                mass(QtySiMass())
        {}


    protected:
        unsigned int z;
        std::string  symbol;
        std::string  name;
       QtySiMass  mass;

    public:
        unsigned int getZ() const
        {
            return z;
        }

        const std::string &getSymbol() const
        {
            return symbol;
        }

        const std::string &getName() const
        {
            return name;
        }

        const QtySiMass&getMass() const
        {
            return mass;
        }

    public:
        bool operator< (const Element &rhs) const;
        bool operator> (const Element &rhs) const;
        bool operator<=(const Element &rhs) const;
        bool operator>=(const Element &rhs) const;
        bool operator==(const Element &rhs) const;
        bool operator!=(const Element &rhs) const;
    };
}



