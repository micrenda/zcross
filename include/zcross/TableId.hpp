#pragma once

#include <string>
#include <ostream>
#include <vector>
#include "PrintMode.hpp"

namespace dfpe
{
    class TableId
    {

    public:
        TableId() = default;

        TableId(const std::string &databaseId, const std::string &groupId, int processId) : databaseId(databaseId), groupId(groupId), processId(processId) {}
        TableId(std::string label, std::vector<TableId> parents) : label(std::move(label)), parents(std::move(parents)) {}

        const std::string &getDatabaseId() const     { return databaseId; }
        const std::string &getGroupId() const        { return groupId; }
        int getProcessId() const                     { return processId; }

        void setDatabaseId(const std::string &databaseId)  { TableId::databaseId = databaseId;        }
        void setGroupId(const std::string &groupId)        { TableId::groupId = groupId;              }
        void setProcessId(int processId)                   { TableId::processId = processId;          }

        bool operator==(const TableId &rhs) const;
        bool operator!=(const TableId &rhs) const;

        bool operator< (const TableId &rhs) const;
        bool operator> (const TableId &rhs) const;
        bool operator<=(const TableId &rhs) const;
        bool operator>=(const TableId &rhs) const;

        friend std::ostream &operator<<(std::ostream &os, const TableId& other);

    public:
        bool isDerived() const {return !parents.empty(); }
        std::string toString(const PrintMode& printMode = PrintMode::BASIC) const;

    protected:
        std::string databaseId;
        std::string groupId;
        int         processId = 0;


        std::string label;             // Label is used for tables derived from other tables.
        std::vector<TableId> parents;  // Used only for derived tables
    };

    std::ostream &operator<<(std::ostream &os, const TableId& other);
}
