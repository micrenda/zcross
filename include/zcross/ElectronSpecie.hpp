#pragma once

#include "ZCrossTypes.hpp"
#include <string>
#include <ostream>
#include "PrintMode.hpp"
#include "ParticleSpecie.hpp"
#include <boost/units/systems/si/codata/electron_constants.hpp>
namespace dfpe
{

    class ElectronSpecie: public ParticleSpecie
	{
	public:
        ElectronSpecie(): ParticleSpecie(11, "e", -1, boost::units::si::constants::codata::m_e) { };
	};
}
