#pragma once
#include "ZCrossTypes.hpp"
#include <map>
#include "OutOfTableMode.hpp"
#include "BaseTable.hpp"

namespace dfpe
{
    class DifScatteringTable : public BaseTable
    {
		
    public:
    
    	DifScatteringTable(){};
		virtual ~DifScatteringTable(){};

		QtySiAreaPerSolidAngle getInterpoledValue(
			const QtySiEnergy&     energy, 
			const QtySiPlaneAngle& angle, 
			const OutOfTableMode&  lowerBoundaryMode = OutOfTableMode::LAUNCH_EXCEPTION, 
			const OutOfTableMode&  upperBoundaryMode = OutOfTableMode::LAUNCH_EXCEPTION) const;
        
        void addEntry(
			const QtySiEnergy& 				energy,  
			const QtySiPlaneAngle&  		angle,  
			const QtySiAreaPerSolidAngle&	section);
        
        virtual unsigned int getSize() const override;
        virtual QtySiEnergy getEnergyLowerBound() const override;
		virtual QtySiEnergy getEnergyUpperBound() const override;

        QtySiEnergy getEnergyThreshold() const override;

        virtual std::string getTableContent(const PrintMode &mode) const override;

        TableType getProcessTableType() const override;

    protected:
		std::map<QtySiEnergy,std::map<QtySiPlaneAngle,QtySiAreaPerSolidAngle>> table;
	};
}

