#pragma once

#include <string>

namespace dfpe
{
    enum class OutOfTableMode
    {
        LAUNCH_EXCEPTION,
        BOUNDARY_VALUE,
        ZERO_VALUE,
        FIT_VALUE
    };
}
