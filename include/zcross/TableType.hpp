#pragma once

#include <string>

namespace dfpe
{
    enum class TableType
    {
        INTEGRAL, DIFFERENTIAL
    };

    class TableTypeUtil
    {
    public:
        static const std::string   getName(const TableType& type);
        static const std::string   getDesc(const TableType& type);
        static TableType    parse(const std::string& s);
    };
}
