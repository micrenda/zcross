#pragma once

#include <string>

namespace dfpe
{
    enum class ProcessCollisionType
    {
       TOTAL, ELASTIC, INELASTIC, SUPERELASTIC
    };

    class ProcessCollisionUtil
    {
    public:
        static const std::string        getName(const ProcessCollisionType& type);
        static const std::string        getDesc(const ProcessCollisionType& type);
        static ProcessCollisionType     parse(const std::string& s);
    };
}
