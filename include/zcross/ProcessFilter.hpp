#pragma once

#include <vector>
#include <string>
#include "BaseTable.hpp"
#include "Specie.hpp"
#include "ProcessCollisionType.hpp"
#include "ProcessCollisionInelasticType.hpp"
#include "ProcessDirectionType.hpp"
#include "ProcessMomentOrder.hpp"

namespace dfpe
{
	class BaseProcessFilter
	{
		public:
			virtual bool accept(const BaseTable& table) const = 0;			
	};
	
	
	class ProcessFilter: public BaseProcessFilter
	{
	public:
		ProcessFilter(std::string s, bool exact = true);
		ProcessFilter(bool exact = true): exact(exact) {};

		virtual ~ProcessFilter(){};
		
	protected:
		std::vector<Specie> 		bullets;
		std::vector<Specie> 		targets;
		
		std::vector<std::string> 	databaseIds;
		std::vector<std::string>	groupIds;
		std::vector<int>		processIds;
		
		std::vector<ProcessCollisionType>			processCollisionTypes;
		std::vector<ProcessCollisionInelasticType>	processCollisionInelasticTypes;
		std::vector<ProcessDirectionType>			processDirectionTypes;
		std::vector<int>							processMomentOrders;

	public:
	    bool hasFilterByBullet() 		const { return !bullets.empty(); 		}
	    bool hasFilterByTarget() 		const { return !targets.empty(); 		}
	    bool hasFilterByDatabaseId()    const { return !databaseIds.empty(); 	}
	    bool hasFilterByGroupId()   	const { return !groupIds.empty(); 		}
	    bool hasFilterByProcessId()   	const { return !processIds.empty(); 		}
	    
	    bool hasFilterByProcessCollisionType()   			const { return !processCollisionTypes.empty(); 	}
	    bool hasFilterByProcessCollisionInelasticType()   	const { return !processCollisionInelasticTypes.empty(); 	}
	    bool hasFilterByProcessDirectionType()   			const { return !processDirectionTypes.empty(); 	}
	    bool hasFilterByProcessMomentOrder()   				const { return !processMomentOrders.empty(); 	}

	    
	public:
	    const std::vector<Specie>& 		getFilterByBullet() 							const { return bullets; 		}
	    const std::vector<Specie>& 		getFilterByTarget() 							const { return targets; 		}
	    const std::vector<std::string>&	getFilterByDatabaseId()    						const { return databaseIds; 	}
	    const std::vector<std::string>&	getFilterByGroupId()   							const { return groupIds; 		}
	    const std::vector<int>&			getFilterByProcessId()   						const { return processIds; 		}
	    
	    const std::vector<ProcessCollisionType>& 			getFilterByProcessCollisionType()   			const { return processCollisionTypes; 	}
	    const std::vector<ProcessCollisionInelasticType>& 	getFilterByProcessCollisionInelasticType()   	const { return processCollisionInelasticTypes; 	}
	    const std::vector<ProcessDirectionType>& 			getFilterByProcessDirectionType()   			const { return processDirectionTypes; 	}
	    const std::vector<int>& 								getFilterByProcessMomentOrder()   				const { return processMomentOrders; 	}

	    
	
	public:
	    void addFilterByBullet(const Specie& value) 			{ bullets.push_back(value);      }
	    void addFilterByTarget(const Specie& value) 			{ targets.push_back(value);      }
	    void addFilterByDatabaseId(const std::string& value)   	{ databaseIds.push_back(value);  }
	    void addFilterByGroupId(const std::string& value)   	{ groupIds.push_back(value);     }
	    void addFilterByProcessId(const int& value)   			{ processIds.push_back(value);     }
        
	    void addFilterByProcessCollisionType(const ProcessCollisionType& value) 			        { processCollisionTypes.push_back(value); }
	    void addFilterByProcessCollisionInelasticType(const ProcessCollisionInelasticType& value)   { processCollisionInelasticTypes.push_back(value); }
	    void addFilterByProcessDirectionType(const ProcessDirectionType& value) 			        { processDirectionTypes.push_back(value); }
	    void addFilterByProcessMomentOrder(int value) 			        							{ processMomentOrders.push_back(value); }
	    void addFilterByProcessMomentOrder(const ProcessMomentOrderType& value) 			        { processMomentOrders.push_back(ProcessMomentOrderUtil::getValue(value)); }

	public:
	    void addFilterByBullets(const std::vector<Specie>& values) 				{ bullets.insert(bullets.end(), 			values.begin(), values.end()); }
	    void addFilterByTargets(const std::vector<Specie>& values) 				{ targets.insert(targets.end(),				values.begin(), values.end()); }
	    void addFilterByDatabaseIds(const std::vector<std::string>& values) 	{ databaseIds.insert(databaseIds.end(),		values.begin(), values.end()); }
	    void addFilterByGroupIds(const std::vector<std::string>& values)   		{ groupIds.insert(groupIds.end(),			values.begin(), values.end()); }
	    void addFilterByProcessIds(const std::vector<int>& values)   			{ processIds.insert(processIds.end(),		values.begin(), values.end()); }
	    
        void addFilterByProcessCollisionTypes(const std::vector<ProcessCollisionType>& values) 			            { processCollisionTypes.insert(processCollisionTypes.end(),	values.begin(), values.end()); }
	    void addFilterByProcessCollisionInelasticTypes(const std::vector<ProcessCollisionInelasticType>& values) 	{ processCollisionInelasticTypes.insert(processCollisionInelasticTypes.end(),	values.begin(), values.end()); }
	    void addFilterByProcessDirectionTypes(const std::vector<ProcessDirectionType>& values) 			            { processDirectionTypes.insert(processDirectionTypes.end(),	values.begin(), values.end()); }
	    void addFilterByProcessMomentOrderTypes(const std::vector<int>& values) 			            			{ processMomentOrders.insert(processMomentOrders.end(),	values.begin(), values.end()); }
	    void addFilterByProcessMomentOrderTypes(const std::vector<ProcessMomentOrderType>& values) 			        { for (const ProcessMomentOrderType& value: values) processMomentOrders.push_back(ProcessMomentOrderUtil::getValue(value)); }


    public:
        void clearFilterByBullet() 			{ bullets.clear();      }
        void clearFilterByTarget() 			{ targets.clear();      }
        void clearFilterByDatabaseId()   	{ databaseIds.clear();  }
        void clearFilterByGroupId()   	    { groupIds.clear();     }

        void clearFilterByProcessCollisionType() 			{ processCollisionTypes.clear(); }
        void clearFilterByProcessCollisionInelasticType()   { processCollisionInelasticTypes.clear(); }
        void clearFilterByProcessDirectionType() 			{ processDirectionTypes.clear(); }
        void clearFilterByProcessMomentOrder() 			{ processMomentOrders.clear(); }

	public:
		virtual bool accept(const BaseTable& table) const override;
	    
	    
	private:
		bool exact;
	    
	};
	
	
	class ProcessFilterAny: public BaseProcessFilter
	{
	public:
		ProcessFilterAny() {};
		
		~ProcessFilterAny(){};
		
	protected:
		std::vector<ProcessFilter> 		filters;
		
	public:
        void addFilter(const ProcessFilter& value) 			    	{ filters.push_back(value); }
	    void addFilters(const std::vector<ProcessFilter>& values) 	{ filters.insert(filters.end(),	values.begin(), values.end()); }
	
	public:
		const std::vector<ProcessFilter>& getFilters() const { return filters; };
        void clearFilters() 			{ filters.clear();      }

	public:
		virtual bool accept(const BaseTable& table) const override;

	};
	
	class ProcessFilterAll: public BaseProcessFilter
	{
	public:
		ProcessFilterAll() {};
		
		~ProcessFilterAll(){};
		
	protected:
		std::vector<ProcessFilter> 		filters;
		
	public:
        void add(const ProcessFilter& value) 			  	{ filters.push_back(value); }
	    void add(const std::vector<ProcessFilter>& values) 	{ filters.insert(filters.end(),	values.begin(), values.end()); }
	
	public:
        void clear() 			{ filters.clear();      }

	public:
		virtual bool accept(const BaseTable& table) const override;

	};

    class TableIdFilter: public BaseProcessFilter
    {
    public:
        TableIdFilter() {};

        ~TableIdFilter(){};

    protected:
        std::vector<TableId> 		tableIds;

    public:
        void add(const TableId& value) 			    	{ tableIds.push_back(value); }
        void add(const std::vector<TableId>& values) 	{ tableIds.insert(tableIds.end(),	values.begin(), values.end()); }

    public:
        const std::vector<TableId>& getTableIds() const { return tableIds; };
        void clear() 			{ tableIds.clear();      }

    public:
        virtual bool accept(const BaseTable& table) const override;

    };
}
