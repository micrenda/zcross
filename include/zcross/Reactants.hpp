#pragma once

#include <optional>
#include <vector>
#include "Specie.hpp"

namespace dfpe
{
    class Reactants
    {

    protected:
        Specie bullet;
        Specie target;

    public:

        const Specie &getBullet() const
        {
            return bullet;
        }

        void setBullet(const Specie &bullet)
        {
            Reactants::bullet = bullet;
        }

        const Specie &getTarget() const
        {
            return target;
        }

        void setTarget(const Specie &target)
        {
            assert(target.isIon());
            Reactants::target = target;
        }

        const std::string toString(const PrintMode &mode) const;

        int getElectronsCount() const;

        int getIonsCount() const;

        int getSpecieCount(const Specie specie, bool ignoreCharge, bool ignoreState) const;
    };

}
