#pragma once

#include <functional>



namespace dfpe {

    class Specie;
    class Element;
    class ParticleSpecie;
    class IonSpecie;
    class Molecule;
    class MoleculeComponent;
    class Reference;
}

namespace std
{
    template <>
    struct hash<const dfpe::Specie>
    {
        std::size_t operator()(const dfpe::Specie& k) const;
    };

    template <>
    struct hash<dfpe::Specie>
    {
        std::size_t operator()(const dfpe::Specie& k) const;
    };

    // ---
    
    template <>
    struct hash<dfpe::Element>
    {
        std::size_t operator()(const dfpe::Element& k) const;
    };

    template <>
    struct hash<dfpe::ParticleSpecie>
    {
        std::size_t operator()(const dfpe::ParticleSpecie& k) const;
    };
    
    template <>
    struct hash<dfpe::IonSpecie>
    {
        std::size_t operator()(const dfpe::IonSpecie& k) const;
    };

    template <>
    struct hash<dfpe::Molecule>
    {

        std::size_t operator()(const dfpe::Molecule& k) const;
    };

    template <>
    struct hash<dfpe::MoleculeComponent>
    {
        std::size_t operator()(const dfpe::MoleculeComponent& k) const;
    };

    template <>
    struct hash<dfpe::Reference>
    {
        std::size_t operator()(const dfpe::Reference& k) const;
    };
}
