#pragma once

namespace dfpe
{
    enum class MoleculePolarType
    {
        NON_POLAR, POLAR
    };
}
