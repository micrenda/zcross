#pragma once

#include "ZCrossTypes.hpp"


namespace dfpe
{
    class Version
    {
    private:
        int major, minor, revision, build;

    public:
        Version(): major(0), minor(0), revision(0), build(0)
        {}

        Version(const std::string& version): major(0), minor(0), revision(0), build(0)
        {
            std::sscanf(version.c_str(), "%d.%d.%d.%d", &major, &minor, &revision, &build);

            if (major < 0) major = 0;
            if (minor < 0) minor = 0;
            if (revision < 0) revision = 0;
            if (build < 0) build = 0;
        }

        bool operator<(const Version &rhs) const
        {
            if (major < rhs.major)
                return true;
            if (rhs.major < major)
                return false;
            if (minor < rhs.minor)
                return true;
            if (rhs.minor < minor)
                return false;
            if (revision < rhs.revision)
                return true;
            if (rhs.revision < revision)
                return false;
            return build < rhs.build;
        }

        bool operator>(const Version &rhs) const
        {
            return rhs < *this;
        }

        bool operator<=(const Version &rhs) const
        {
            return !(rhs < *this);
        }

        bool operator>=(const Version &rhs) const
        {
            return !(*this < rhs);
        }

        bool isEmpty() const
        {
            return major == 0 && minor == 0 && revision == 0 && build == 0;
        }

        const std::string toString() const
        {
            std::string s = std::to_string(major) + "." + std::to_string(minor);

            if (revision > 0 || build > 0)
                s += "." + std::to_string(revision);
            if (build > 0)
                s += "." + std::to_string(build);

            return s;
        }

        bool operator==(const Version& other) const
        {
            return major == other.major
                   && minor == other.minor
                   && revision == other.revision
                   && build == other.build;
        }

        bool operator!=(const Version &rhs) const
        {
            return !(rhs == *this);
        }
    };


}




