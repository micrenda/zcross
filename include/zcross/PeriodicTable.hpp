#pragma once

#include <filesystem>
#include <map>
#include <string>

#include "Element.hpp"

namespace dfpe
{
    class PeriodicTable
    {
    public:
		PeriodicTable();
    
        const Element& parseElement(const std::string& symbol) const;

    protected:
        void load(std::filesystem::path filePath);
        std::map<const std::string, Element> elements;
		
		std::filesystem::path tablePath;
		std::filesystem::path isotopesPath;
		
    public:
        static const PeriodicTable&  getInstance();
        
    protected:
        static       PeriodicTable   instance;
        
    };
}


