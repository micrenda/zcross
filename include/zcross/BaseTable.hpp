#pragma once

#include "ZCrossTypes.hpp"
#include <chrono>
#include <string>

#include "Reaction.hpp"
#include "PrintMode.hpp"
#include "GroupSourceType.hpp"
#include "ProcessCollisionType.hpp"
#include "ProcessCollisionInelasticType.hpp"
#include "ProcessDirectionType.hpp"
#include "ProcessMomentOrder.hpp"
#include "TableType.hpp"
#include "Process.hpp"
#include "TableId.hpp"

namespace dfpe
{
    class BaseTable
    {
    protected:

        TableId tableId;

        std::string databaseName;
        std::string databaseUrl;
        std::string databaseDescription;
        std::string databaseContacts;

        std::string 		groupDescription;       
		GroupSourceType     groupSourceType;
		std::string         groupSourceMethod;

        Process         process;

        std::chrono::time_point<std::chrono::system_clock>	processLastUpdate;

		std::vector<int> databaseReferenceIds;
		std::vector<int> groupReferenceIds;
		std::vector<int> processReferenceIds;

    public:
        const std::string& getDatabaseId() const							{ return tableId.getDatabaseId(); };
        const std::string& getDatabaseName() const							{ return databaseName; };
        const std::string& getDatabaseUrl() const							{ return databaseUrl; };
        const std::string& getDatabaseDescription() const   				{ return databaseDescription; };
        const std::string& getDatabaseContacts() const						{ return databaseContacts; };
		const std::vector<int> &getDatabaseReferenceIds() const				{ return databaseReferenceIds; }

		const std::string& getGroupId() const                { return tableId.getGroupId(); }
        const std::string& getGroupDescription() const       { return groupDescription; }
		const std::vector<int> &getGroupReferenceIds() const { return groupReferenceIds; }
		const GroupSourceType& getGroupSourceType() const    { return groupSourceType; }
        const std::string& getGroupSourceMethod() const      { return groupSourceMethod; }

		int getProcessId() const 							{ return tableId.getProcessId();	}
        const std::chrono::time_point<std::chrono::system_clock>& getProcessLastUpdate() const     	{ return processLastUpdate; }
        const Process &getProcess() const { return process; }
		const std::vector<int> &getProcessReferenceIds() const	{ return processReferenceIds; }


		void setDatabaseId(const std::string databaseId)								{ tableId.setDatabaseId(databaseId);           				}
        void setDatabaseName(const std::string databaseName)							{ BaseTable::databaseName 		    = databaseName;        	}
        void setDatabaseUrl(const std::string databaseUrl)								{ BaseTable::databaseUrl 		    = databaseUrl;        	}
        void setDatabaseDescription(const std::string databaseDescription)				{ BaseTable::databaseDescription    = databaseDescription; 	}
        void setDatabaseContacts(const std::string databaseContacts)					{ BaseTable::databaseContacts       = databaseContacts;    	}

		void setDatabaseReferenceIds(const std::vector<int> &databaseReferenceIds)		{ BaseTable::databaseReferenceIds = databaseReferenceIds; }

		void setGroupId(const std::string groupId)								{ tableId.setGroupId(groupId);          }
        void setGroupDescription(const std::string& groupDescription)       	{ BaseTable::groupDescription       = groupDescription; }
		void setGroupReferenceIds(const std::vector<int> &groupReferenceIds)	{ BaseTable::groupReferenceIds = groupReferenceIds;	}
		void setGroupSourceType(const GroupSourceType& groupSourceType) 		{ BaseTable::groupSourceType = groupSourceType; }
        void setGroupSourceMethod(const std::string& groupSourceMethod) 		{ BaseTable::groupSourceMethod = groupSourceMethod; }

        void setProcessId(int processId)								{ tableId.setProcessId(processId);          }
		virtual TableType getProcessTableType() const  = 0;

        void setProcessLastUpdate(const std::chrono::time_point<std::chrono::system_clock> &processLastUpdate)     { BaseTable::processLastUpdate = processLastUpdate; }
        void setProcess(const Process &process) { BaseTable::process = process;  }
		void setProcessReferenceIds(const std::vector<int> &processReferenceIds)		{ BaseTable::processReferenceIds = processReferenceIds;	}



    public:
		const TableId& getId() const;
        const std::string toString(const PrintMode& printMode = PrintMode::BASIC) const;
		std::string getShortDescription() const;

		virtual void validate() const;

		virtual unsigned int getSize() const  = 0;
		
		virtual QtySiEnergy getEnergyLowerBound() const  = 0;
		virtual QtySiEnergy getEnergyUpperBound() const  = 0;
		virtual std::string getTableContent(const PrintMode &mode) const = 0;

        virtual QtySiEnergy getEnergyThreshold() const = 0;

        bool operator==(const BaseTable &rhs) const;
        bool operator!=(const BaseTable &rhs) const;

		bool operator<(const BaseTable &rhs)  const;
		bool operator>(const BaseTable &rhs)  const;
		bool operator<=(const BaseTable &rhs) const;
		bool operator>=(const BaseTable &rhs) const;
    };

}
