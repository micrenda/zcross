#pragma once

#include <string>

namespace dfpe
{
    enum class ProcessCollisionInelasticType
    {
        EXCITATION_ELE, EXCITATION_VIB, EXCITATION_ROT, IONIZATION, ATTACHMENT, NEUTRAL, NONE
    };

    class ProcessCollisionInelasticUtil
    {
    public:
        static const std::string                 getName(const ProcessCollisionInelasticType& type);
        static const std::string                 getDesc(const ProcessCollisionInelasticType& type);
        static ProcessCollisionInelasticType     parse(const std::string& s);
    };
}
