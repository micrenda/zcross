#pragma once

#include <string>

namespace dfpe
{
    enum class ProcessDirectionType
    {
        ISOTROPIC, ANISOTROPIC, BACKSCATTERING, UNKNOWN
    };

    class ProcessDirectionUtil
    {
    public:
		static const std::string 		getNameDefault(const ProcessDirectionType& type, const std::string& defaultValue);
        static const std::string        getName(const ProcessDirectionType& type);
        static const std::string        getDesc(const ProcessDirectionType& type);
        static ProcessDirectionType     parse(const std::string& s);
    };
}
