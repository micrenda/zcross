#pragma once

#include "ZCrossTypes.hpp"
#include <string>
#include <ostream>
#include "PrintMode.hpp"
namespace dfpe
{

	class ParticleSpecie
	{
	public:
		ParticleSpecie(int id, std::string name, int charge, QtySiMass mass): id(id), name(std::move(name)), ionization(charge), mass(mass) { };

	public:
        int                     getId()     const; /// PGDId
		const std::string&	    getName()   const;
		QtySiMass 				getMass()   const;
		QtySiElectricCharge		getCharge() const;
		int						getIonization()	const;

    public:

        bool isLepton()   const { return id > 10 && id < 20; };

        bool isElectron() const { return id ==  11; };
        bool isPositron() const { return id == -11; };
        bool isMuon()     const { return id ==  13; };
        bool isAntiMuon() const { return id == -13; };
        bool isTau()      const { return id ==  15; };
        bool isAntiTau()  const { return id == -15; };

	public:
		const std::string toString(const PrintMode& mode = PrintMode::BASIC)  const;
		friend std::ostream &operator<<(std::ostream &os, const ParticleSpecie &specie);

	public:
        bool operator==(const ParticleSpecie &rhs) const;
        bool operator!=(const ParticleSpecie &rhs) const;
        bool operator< (const ParticleSpecie &rhs) const;

    protected:
        int         id;
        std::string name;
        int         ionization;
        QtySiMass   mass;
	};
}
