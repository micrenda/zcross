#pragma once

#include <filesystem> 
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ptree_fwd.hpp>
#include <unordered_map> 
#include <vector>
#include <type_traits>

#include "Specie.hpp"
#include "Hashes.hpp"
#include "Molecule.hpp"
#include "Reference.hpp"
#include "ProcessFilter.hpp"
#include "IntScatteringTable.hpp"
#include "DifScatteringTable.hpp"
#include "ZCrossTypes.hpp"

namespace dfpe {
class Specie;
class IntScatteringTable;
}  

namespace dfpe
{
	class ZCross
	{
		
	public:
		ZCross();
		~ZCross();


		bool isDatabaseLoaded(std::string databaseId);
		void loadDatabase(const std::string& name);
		void loadDatabases();
		void loadDatabases(const std::vector<std::string>& names);

	protected:
		std::vector<std::filesystem::path> getDataPaths() const;
		void initDatabasesMap();
		std::vector<std::filesystem::path> listDatabasesFiles() const;
		void loadDatabaseFile(const std::filesystem::path &filename);
        void loadXml(boost::property_tree::ptree &rootXml);

    public:
        const std::vector<IntScatteringTable> getIntScatteringTables() const;
        const std::vector<IntScatteringTable> getIntScatteringTables(const BaseProcessFilter& filter) const;
        const std::vector<IntScatteringTable> getIntScatteringTables(const Specie &bulletSpecie) const;
        const std::vector<IntScatteringTable> getIntScatteringTables(const Specie &bulletSpecie, const Specie &targetSpecie) const;

		const std::vector<DifScatteringTable> getDifScatteringTables() const;
		const std::vector<DifScatteringTable> getDifScatteringTables(const BaseProcessFilter& filter) const;
        const std::vector<DifScatteringTable> getDifScatteringTables(const Specie& bulletSpecie) const;
        const std::vector<DifScatteringTable> getDifScatteringTables(const Specie& bulletSpecie, const Specie& targetSpecie) const;

    public:
        const std::set<std::string> getDatabasesIds() const;
        const std::map<std::string,std::set<std::string>> getGroupsIds() const;
        const std::map<std::string,std::map<std::string,std::set<int>>> getProcessesIds() const;
        const std::set<TableId> getTableIds() const;
    protected:

		std::map<std::string,std::filesystem::path>   databasesMap;
		std::map<std::string,bool> 					  databasesLoaded;

		std::unordered_map<
                const Specie,
                std::unordered_map<
                        const Specie,
                        std::vector<IntScatteringTable>
                >
        > intScatteringTables;
        
		std::unordered_map<
                const Specie,
                std::unordered_map<
                        const Specie,
                        std::vector<DifScatteringTable>
                >
        > difScatteringTables;

    protected:
        std::map<std::string, std::map<int,Reference>> references;
    public:
        std::vector<Reference> getDatabaseReferences(const BaseTable& table) const;
        std::vector<Reference> getGroupReferences(const BaseTable& table) const;
        std::vector<Reference> getProcessReferences(const BaseTable& table) const;
    };
}
