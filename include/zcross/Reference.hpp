#pragma once

#include <string>
#include <vector>
#include <boost/regex.hpp>
#include "PrintMode.hpp"

namespace dfpe
{
    class Reference
    {
    public:
        const std::string toString(const PrintMode& mode = PrintMode::BASIC) const;
    protected:
        std::string type;
        std::string label;

        std::vector<std::string> keys;
        std::vector<std::string> values;

        static const boost::regex referenceRegex;
        static const boost::regex fieldRegex;
        static const boost::regex equalRegex;
        static const boost::regex commaRegex;
    public:
        const std::string &getType() const  { return type;  };
        const std::string &getLabel() const { return label; };

        const std::vector<std::string> &getFields() const { return keys; };
        const std::vector<std::string> &getValues() const { return values; };
        
        bool hasField(const std::string& field) const;
        const std::string& getValue(const std::string& field) const;


    public:
        static Reference parse(const std::string& s);

        bool operator==(const Reference &rhs) const;

        bool operator!=(const Reference &rhs) const;
    };



}
