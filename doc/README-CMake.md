# Create an application using CMake

A stardard C++ project using `ZCross` with have this structure:
``` shell
example1
  ├── CMakeLists.txt
  └── src
      └── main.cpp
```

The file `CMakeLists.txt` will contains:

``` cmake
cmake_minimum_required(VERSION 3.0)
project(example1)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -pedantic" )

include_directories(include)

file(GLOB SOURCE_FILES src/*.cpp)
add_executable(example1 ${SOURCE_FILES})

find_package (ZCross REQUIRED HINTS /opt/zcross/share/zcross/cmake/ )
if (${ZCross_FOUND})
    target_link_libraries (example1 PUBLIC DFPE::zcross)
endif()
```

Our project and our executable in this example are named `example1`: you can set it wathever you want.
