# Looking for cross section tables

In this application we show the list of cross section tables existing in Itikawa database, for the group CO2.

File `main.cpp`:

```c++
#include "ZCross.hpp"
#include <iostream>
using namespace dfpe;
using namespace std;
using namespace boost::units;

int main()
{
    ZCross parser;
    parser.loadDatabase("Itikawa");

    ProcessFilter filter;
    filter.addFilterByDatabaseId("Itikawa");
    filter.addFilterByGroupId("CO2");

    for (const IntScatteringTable& table: parser.getIntScatteringTables(filter))
    {
        cout << left
             << setw(20) << table.getId()
             << setw(45) << table.getProcess().getShortDescription()
             << "Comments: "
             << setw(60) << table.getProcess().getComments()
             << setw(40) << table.getProcess().getReaction().toString(PrintMode::PRETTY) << endl;
    }
}

```

Compiling and executing:

``` bash
mkdir build
cmake .. && make
./example1
```
We get the list of cross section tables for database Itikawa:

```
Itikawa/CO2/1       attachment cross-section                     Comments: cross section for the production of O^-.                    e + CO₂ → CO + O⁻                 
Itikawa/CO2/2       elastic cross-section                        Comments: Elastic Scattering Cross section.                           e + CO₂ → e + CO₂                 
Itikawa/CO2/3       elastic cross-section (momentum transfer)    Comments: Momentum Transfer Cross section.                            e + CO₂ → e + CO₂                 
Itikawa/CO2/4       total cross-section                          Comments: Total Scattering Cross section.                             e + CO₂ → e + CO₂                 
Itikawa/CO2/5       excitation cross-section                     Comments: Cross section for O(1S) production.                         e + CO₂ → e + CO₂(O1S)            
Itikawa/CO2/6       excitation (vib) cross-section               Comments: Vibrational Excitation v001.                                e + CO₂ → e + CO₂(V 001)          
Itikawa/CO2/7       excitation (vib) cross-section               Comments: Vibrational Excitations V100.                               e + CO₂ → e + CO₂(v 100)          
Itikawa/CO2/8       excitation (vib) cross-section               Comments: Vibrational Excitations V 010.                              e + CO₂ → e + CO₂(v 010)          
Itikawa/CO2/9       ionization cross-section                     Comments:                                                             e + CO₂ → e + e + CO₂⁺          
Itikawa/CO2/10      ionization cross-section                     Comments: Total Ionization.                                           e + CO₂ → e + e + CO₂⁺(Total Ionization)
Itikawa/CO2/11      dissociative ionization cross-section        Comments: Dissociative ionization cross section.                      e + CO₂ → e + e + CO + O⁺         
Itikawa/CO2/12      dissociative ionization cross-section        Comments: Dissociative ionization cross sections.                     e + CO₂ → e + e + CO⁺ + O         
Itikawa/CO2/13      ionization cross-section                     Comments:                                                             e + CO₂ → e + e + C⁺ + O₂       
Itikawa/CO2/14      ionization cross-section                     Comments: Ionization cross section for the production of CO2++.       e + CO₂ → e + e + e + CO₂²⁺    
Itikawa/CO2/15      dissociative ionization cross-section        Comments: Dissociative ionization cross section.                      e + CO₂ → e + e + e + C²⁺ + O₂ 
Itikawa/CO2/16      dissociative ionization cross-section        Comments: Dissociative ionization cross section.                      e + CO₂ → e + e + e + O²⁺ + CO   
```
