#include "zcross/ZCross.hpp"
#include <iostream>
using namespace dfpe;
using namespace std;
using namespace boost::units;

int main()
{
    ZCross parser;
    parser.loadDatabase("Itikawa");

    ProcessFilter filter;
    filter.addFilterByDatabaseId("Itikawa");
    filter.addFilterByGroupId("CO2");

    for (const IntScatteringTable& table: parser.getIntScatteringTables(filter))
    {
        cout << left
             << setw(20) << table.getId()
             << setw(45) << table.getProcess().getShortDescription()
             << "Comments: "
             << setw(60) << table.getProcess().getComments()
             << setw(40) << table.getProcess().getReaction().toString(PrintMode::PRETTY) << endl;
    }
}
