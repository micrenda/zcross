# Query the value of a specific table

In this application we query a value of a speficic table

File `main.cpp`:

```c++
#include <ZCross.hpp>
#include <iostream>
using namespace dfpe;
using namespace std;
using namespace boost::units;

int main()
{
    ZCross parser;
    parser.loadDatabase("Itikawa");

    ProcessFilter filter;
    filter.addFilterByDatabaseId("Itikawa");
    filter.addFilterByGroupId("CO2");
    filter.addFilterByProcessCollisionType(ProcessCollisionType::ELASTIC);
    filter.addFilterByProcessMomentOrder(0);

    IntScatteringTable table = parser.getIntScatteringTables(filter)[0];
    
    auto eV = ZCrossTypes::electronvoltEnergy;
    
    cout << "Table: "        << table.toString() << endl;
    cout << "Energies from " << (double) (table.getEnergyLowerBound() / eV) << " eV "
         << "up to "         << (double) (table.getEnergyUpperBound() / eV) << " eV." << endl;
    cout << "Cross-section at 420 eV: " << table.getInterpoledValue(420. * eV)        << endl;
}

```

Compiling and executing:

``` bash
mkdir build
cmake .. && make
./example3
```
We get this output:

```
Table: Itikawa/CO2/2 (31 entries)
Energies from 1 eV up to 1000 eV.
Cross-section at 420 eV: 3.308e-20 m^2
```


