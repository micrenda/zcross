#include <zcross/ZCross.hpp>
#include <iostream>
using namespace dfpe;
using namespace std;
using namespace boost::units;

int main()
{
    ZCross parser;
    parser.loadDatabase("Itikawa");

    ProcessFilter filter;
    filter.addFilterByDatabaseId("Itikawa");
    filter.addFilterByGroupId("CO2");
    filter.addFilterByProcessCollisionType(ProcessCollisionType::ELASTIC);
    filter.addFilterByProcessMomentOrder(0);

    IntScatteringTable table = parser.getIntScatteringTables(filter)[0];
    
    auto eV = ZCrossTypes::electronvoltEnergy;
    
    cout << "Table: "        << table.toString() << endl;
    cout << "Energies from " << (double) (table.getEnergyLowerBound() / eV) << " eV "
         << "up to "         << (double) (table.getEnergyUpperBound() / eV) << " eV." << endl;
    cout << "Cross-section at 420 eV: " << table.getInterpoledValue(420. * eV)        << endl;
}
