#include <zcross/ZCross.hpp>
#include <iostream>
using namespace dfpe;
using namespace std;
using namespace boost::units;

int main()
{
    ZCross parser;
    parser.loadDatabase("Itikawa");

    ProcessFilter filter;
    filter.addFilterByDatabaseId("Itikawa");
    filter.addFilterByGroupId("CO2");

    for (const IntScatteringTable& table:  parser.getIntScatteringTables(filter))
    {
		cout << table.toString() << endl;
		for (const Reference& ref: parser.getDatabaseReferences(table))
			cout << "  Database reference: " << ref.toString() << endl;
		for (const Reference& ref: parser.getGroupReferences(table))
			cout << "  Group reference: "    << ref.toString() << endl;
		for (const Reference& ref: parser.getProcessReferences(table))
			cout << "  Process reference: "  << ref.toString() << endl;
	}
}
