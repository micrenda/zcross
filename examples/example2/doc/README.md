# Printing the references

In this application print the references of cross section tables of the Itikawa database, for the group CO2.

File `main.cpp`:

```c++
#include <ZCross.hpp>
#include <iostream>
using namespace dfpe;
using namespace std;
using namespace boost::units;

int main()
{
    ZCross parser;
    parser.loadDatabase("Itikawa");

    ProcessFilter filter;
    filter.addFilterByDatabaseId("Itikawa");
    filter.addFilterByGroupId("CO2");

    for (const IntScatteringTable& table:  parser.getIntScatteringTables(filter))
    {
		cout << table.toString() << endl;
		for (const Reference ref: parser.getDatabaseReferences(table))
			cout << "  Database reference: " << ref.toString() << endl;
		for (const Reference ref: parser.getGroupReferences(table))
			cout << "  Group reference: "    << ref.toString() << endl;
		for (const Reference ref: parser.getProcessReferences(table))
			cout << "  Process reference: "  << ref.toString() << endl;
	}
}

```

Compiling and executing:

``` bash
mkdir build
cmake .. && make
./example2
```
We get the list of references:

```
Itikawa/CO2/1 (68 entries)
  Group reference: itikawa2002cross (article)
           title = Cross {{Sections}} for {{Electron Collisions With Carbon Dioxide}}
          volume = 31
            issn = 0047-2689
             doi = 10.1063/1.1481879
       timestamp = 2017-08-31T09:57:38Z
          number = 3
         urldate = 2017-08-31
         journal = Journal of Physical and Chemical Reference Data
          author = Itikawa, Yukikazu
           month = jul
            year = 2002
           pages = 749--767

Itikawa/CO2/2 (31 entries)
  Group reference: itikawa2002cross (article)
           title = Cross {{Sections}} for {{Electron Collisions With Carbon Dioxide}}
          volume = 31
            issn = 0047-2689
             doi = 10.1063/1.1481879
       timestamp = 2017-08-31T09:57:38Z
          number = 3
         urldate = 2017-08-31
         journal = Journal of Physical and Chemical Reference Data
          author = Itikawa, Yukikazu
           month = jul
            year = 2002
           pages = 749--767

Itikawa/CO2/3 (52 entries)
  Group reference: itikawa2002cross (article)
           title = Cross {{Sections}} for {{Electron Collisions With Carbon Dioxide}}
          volume = 31
            issn = 0047-2689
             doi = 10.1063/1.1481879
       timestamp = 2017-08-31T09:57:38Z
          number = 3
         urldate = 2017-08-31
         journal = Journal of Physical and Chemical Reference Data
          author = Itikawa, Yukikazu
           month = jul
            year = 2002
           pages = 749--767
           
(continue...)
```

